package com.example.demo.auth;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class AuthenticationManagerInterceptor extends HandlerInterceptorAdapter {

    private SecurityContextHolder securityContextHolder;

    private List<String> uriIsAuthenticated = new ArrayList<>(
            Arrays.asList(
                    "/facility",
                    "/shopping_cart",
                    "/shopping_cart/add_product",
                    "/appointment",
                    "/appointment/after_now",
                    "/order"
            )
    );

    public AuthenticationManagerInterceptor(SecurityContextHolder securityContextHolder) {
        this.securityContextHolder = securityContextHolder;
    }

    /**
     * If the requested uri is one of the  `uriIsAuthenticated` then i check if the token is present in headers
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        for (String uri: this.uriIsAuthenticated) {
            if (request.getRequestURI().equals(uri)) {
                String token = request.getHeader(Constants.LOOK_UP_HEADER_TOKEN);
                this.securityContextHolder.getUserAfterToken(token);
            }
        }

        return super.preHandle(request, response, handler);
    }
}
