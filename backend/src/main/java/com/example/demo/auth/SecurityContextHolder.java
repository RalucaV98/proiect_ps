package com.example.demo.auth;


import com.example.demo.entities.User;
import com.example.demo.exceptions.InvalidToken;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SecurityContextHolder {

    private List<Token> tokens = new ArrayList<>();

    public SecurityContextHolder() {
    }

    /**
     * generate a token for the givven user
     * @param user
     * @return String The generated token
     */
    public String generateAndAddToken(User user) {
        for (Token t : this.tokens) {
            if (t.getUser().getId().equals(user.getId())) {
                return t.getValue();
            }
        }


        Token token = new Token(TokenGenerator.generateToken(10), user);

        this.tokens.add(token);

        return token.getValue();
    }

    /**
     * Return the user after the given token
     * @param token String
     * @return User The logged user
     */
    public User getUserAfterToken(String token) throws InvalidToken {
        for (Token t: this.tokens) {
            if (t.getValue().equals(token)) {
                return t.getUser();
            }
        }

        throw new InvalidToken();
    }

    /**
     * Remove token object from list
     * @param token
     */
    public void removeToken(String token) throws InvalidToken {
        this.getUserAfterToken(token);

        this.tokens = this.tokens.stream()
                .filter(t -> !t.getValue().equals(token))
                .collect(Collectors.toList());
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }
}
