package com.example.demo.auth;

import com.example.demo.entities.User;

public class Token {

    private String value;

    private User user;


    public Token() {
    }

    public Token(String value, User user) {
        this.value = value;
        this.user = user;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
