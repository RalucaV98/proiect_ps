package com.example.demo.auth;

import java.util.Random;

public class TokenGenerator {

    /**
     * Generate a random string
     * @param stringLength
     * @return String
     */
    public static String generateToken(int stringLength) {

        int leftLimit = 97;
        int rightLimit = 122;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(stringLength);
        for (int i = 0; i < stringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }

        return buffer.toString();
    }
}
