package com.example.demo.config;

import com.example.demo.auth.AuthenticationManagerInterceptor;
import com.example.demo.auth.SecurityContextHolder;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class AppConfig extends WebMvcConfigurerAdapter {

    private SecurityContextHolder securityContextHolder;

    public AppConfig(SecurityContextHolder securityContextHolder) {
        this.securityContextHolder = securityContextHolder;
    }

    /**
     * Add a custom interceptor
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthenticationManagerInterceptor(this.securityContextHolder));
    }
}
