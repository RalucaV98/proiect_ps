package com.example.demo.controller;

import com.example.demo.report.ReportFacade;
import com.example.demo.report.ReportFactory;
import com.example.demo.report.ReportType;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private ReportFacade reportFacade;

    public AdminController(ReportFacade reportFacade) {
        this.reportFacade = reportFacade;
    }

    /**
     * Endpoint for testing Facade DP
     *
     * @param reportType
     */
    @GetMapping(value = "report/{type}/")
    @ResponseBody
    public HttpEntity<byte[]> generateReport(@PathVariable("type") ReportType reportType) throws IOException {
        File file = null;

        if (reportType.equals(ReportType.XLS)) {
            file = new File(this.reportFacade.generateXlxReport());
        } else {
            file = new File(this.reportFacade.generateTxtReport());
        }
        byte[] document = FileCopyUtils.copyToByteArray(file);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", (reportType.equals(ReportType.XLS) ? "xls" : "txt")));
        header.set("Content-Disposition", "attachment; filename=" + file.getName());
        header.setContentLength(document.length);

        return new HttpEntity<byte[]>(document, header);
    }
}
