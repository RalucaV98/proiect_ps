package com.example.demo.controller;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.entities.Appointment;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.service.appointment.AppointmentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/appointment")
public class AppointmentController {

    private AppointmentService appointmentService;

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    /**
     * Endpoint for listing all appointments
     * @return
     */
    @GetMapping
    public List<AppointmentDto> getAllAppointments() {
        return this.appointmentService.getAll();
    }

    /**
     * Endpoint for listing all appointments that are in the future
     * @return
     */
    @GetMapping(value = "/after_now")
    public List<AppointmentDto> getAllAppointmentsFromTheFuture() {
        return this.appointmentService.getAllWhereDateIsGreaterThanToday();
    }

    /**
     * Endpoint for saving an appointment
     */
    @PostMapping
    public AppointmentDto createAppointment(@RequestBody AppointmentDto appointment) throws ValidationError {
        return this.appointmentService.createAppointment(appointment);
    }
}
