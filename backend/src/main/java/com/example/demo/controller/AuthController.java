package com.example.demo.controller;

import com.example.demo.auth.Constants;
import com.example.demo.dto.TokenDto;
import com.example.demo.exceptions.InvalidCredentials;
import com.example.demo.exceptions.InvalidToken;
import com.example.demo.service.user.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Endpoint for login
     * Will return a unique generated token
     * @param username
     * @param password
     * @return
     * @throws InvalidCredentials
     */
    @PostMapping("/token")
    public TokenDto login(String username, String password) throws InvalidCredentials {
        return this.userService.logIn(username, password);
    }

    /**
     * Delete token if exist
     * @param token
     * @throws InvalidToken if token does not exist
     */
    @PostMapping("/revoke")
    public void logout(@RequestHeader(Constants.LOOK_UP_HEADER_TOKEN) String token) throws InvalidToken {
        this.userService.logOut(token);
    }
}
