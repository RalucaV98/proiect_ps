package com.example.demo.controller;

import com.example.demo.dto.CategoryDto;
import com.example.demo.mapper.CategoryMapper;
import com.example.demo.observer.MessageLogger;
import com.example.demo.observer.v2.MessageLoggerV2;
import com.example.demo.service.category.CategoryService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/category")
public class CategoryController {

    private CategoryService categoryService;
    private CategoryMapper categoryMapper;
    private MessageLogger messageLogger;
    private MessageLoggerV2 messageLoggerV2;


    public CategoryController(CategoryService categoryService, CategoryMapper categoryMapper,
                              MessageLogger messageLogger, MessageLoggerV2 messageLoggerV2) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.messageLogger = messageLogger;
        this.messageLoggerV2 = messageLoggerV2;
    }

    /**
     * List endpoint for categories
     * GET /category
     * @return List<CategoryDto>
     */
    @GetMapping
    public List<CategoryDto> getAll() {
        this.messageLogger.writeLog("GET /category");
        this.messageLoggerV2.log("GET /category");
        return categoryService.getAllCategories().stream()
                .map(o -> this.categoryMapper.entityToDto(o))
                .collect(Collectors.toList());
    }

    /**
     * Endpoint for creating a category
     * POST /category
     * @param category
     * @return CategoryDto
     */
    @PostMapping
    public CategoryDto save(@RequestBody CategoryDto category) {
        return this.categoryMapper.entityToDto(this.categoryService.save(category));
    }

    /**
     * Retrieve a category after id
     * GET /category/{id}
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}")
    public CategoryDto getByID(@PathVariable("id") Long id) {
        return this.categoryMapper.entityToDto(this.categoryService.getAfterId(id));
    }

    /**
     * Delete a category from db after id
     * DELETE /category/{id}
     * @param id
     */
    @DeleteMapping(value = "/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        this.categoryService.deleteById(id);
    }
}
