package com.example.demo.controller;

import com.example.demo.dto.OrderDto;
import com.example.demo.exceptions.PaymentFailed;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.payment.CreditCardStrategy;
import com.example.demo.payment.PayPalStrategy;
import com.example.demo.payment.PaymentStrategy;
import com.example.demo.service.order.OrderService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * Endpoint for creating an order
     * @param shoppingCartId
     * @return
     * @throws ValidationError
     */
    @PostMapping
    public OrderDto placeOrder(Long shoppingCartId) throws ValidationError  {
        System.out.println(shoppingCartId);
        return this.orderService.placeOrder(shoppingCartId);
    }

    /**
     * Pay order by PayPal
     * @param paymentStrategy
     * @param orderId
     * @return
     * @throws ValidationError
     * @throws PaymentFailed
     */
    @PostMapping(value = "/pay/paypal/{orderId}")
    public PaymentStrategy payOrderByPayPal(@RequestBody PayPalStrategy paymentStrategy, @PathVariable("orderId") Long orderId) throws ValidationError, PaymentFailed {
        return this.orderService.payOrder(paymentStrategy, orderId);
    }

    /**
     * Pay order by Card
     * @param creditCardStrategy
     * @param orderId
     * @return
     * @throws ValidationError
     * @throws PaymentFailed
     */
    @PostMapping(value = "/pay/card/{orderId}")
    public PaymentStrategy payOrderByCard(@RequestBody CreditCardStrategy creditCardStrategy, @PathVariable("orderId") Long orderId) throws ValidationError, PaymentFailed {
        return this.orderService.payOrder(creditCardStrategy, orderId);
    }
}
