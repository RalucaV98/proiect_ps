package com.example.demo.controller;

import com.example.demo.dto.ProductDto;
import com.example.demo.exceptions.InvalidMediaType;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.service.filestorage.ImageService;
import com.example.demo.service.product.ProductService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ProductController {
    private ProductService productService;
    private ProductMapper productMapper;
    private ImageService imageService;

    public ProductController(ProductService productService, ProductMapper productMapper, ImageService imageService) {
        this.productService = productService;
        this.productMapper = productMapper;
        this.imageService = imageService;
    }

    /**
     * Enpoint for listing all products from database
     * GET /product
     *
     * @return
     */
    @GetMapping
    public List<ProductDto> getAll() {
        return this.productService.getAll()
                .stream()
                .map(o -> this.productMapper.entityToDto(o))
                .collect(Collectors.toList());
    }

    /**
     * Retrive a product after id
     * GET /product/{id}
     *
     * @param id
     * @return ProductDto
     */
    @GetMapping(value = "/{id}")
    public ProductDto getById(@PathVariable("id") Long id) {
        return this.productMapper.entityToDto(this.productService.getById(id));
    }

    /**
     * Endpoint for deleting a product after id
     * DELETE /product/{id}
     *
     * @param id
     */
    @DeleteMapping(value = "{id}")
    public void deleteById(@PathVariable("id") Long id) {
        this.productService.deleteById(id);
    }

    /**
     * Endpoint for create or update a product
     * POST /product
     *
     * @param productDto
     * @return ProductDto
     */
    @PostMapping
    public ProductDto save(@RequestBody ProductDto productDto) {
        return this.productMapper.entityToDto(this.productService.saveProduct(productDto));
    }

    /**
     * Endpoint for updating product image
     */
    @PostMapping(value = "/update_image")
    public ProductDto uploadImage(@RequestParam("image") MultipartFile file, @RequestParam("productId") Long productId) throws InvalidMediaType, IOException, ValidationError {
        return this.productMapper.entityToDto(this.imageService.updateProductImage(file, productId));
    }
}
