package com.example.demo.controller;

import com.example.demo.auth.Constants;
import com.example.demo.dto.AddProductToCartBody;
import com.example.demo.dto.ShoppingCartDto;
import com.example.demo.exceptions.InvalidToken;
import com.example.demo.exceptions.UnauthorizedException;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.service.shoppingcart.ShoppingCartService;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/shopping_cart")
public class ShoppingCartController {

    private ShoppingCartService shoppingCartService;

    public ShoppingCartController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    /**
     * Endpoint for retrieve user active cart
     * @param token
     * @throws UnauthorizedException
     * @throws InvalidToken
     */
    @GetMapping
    public ShoppingCartDto getUserShoppingCart(@RequestHeader(Constants.LOOK_UP_HEADER_TOKEN) String token) throws UnauthorizedException, InvalidToken {
        return this.shoppingCartService.getUserShoppingCart(token);
    }

    /**
     * Endpoint for adding product to a cart
     * @param shoppingCartId
     * @param productId
     * @return
     * @throws ValidationError
     */
    @PostMapping(value = "/add_product")
    public ShoppingCartDto addProductToCart(@RequestBody AddProductToCartBody body) throws ValidationError {
        return this.shoppingCartService.addProductToCart(body.getShoppingCartId(), body.getProductId());
    }

    @PostMapping(value = "/remove_product")
    public ShoppingCartDto removeProductFromCart(Long shoppingCartId, Long productId) throws ValidationError {
        return this.shoppingCartService.removeProductFromCart(shoppingCartId, productId);
    }
}
