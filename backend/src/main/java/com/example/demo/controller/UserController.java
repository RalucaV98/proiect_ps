package com.example.demo.controller;

import com.example.demo.auth.Constants;
import com.example.demo.dto.UserViewDto;
import com.example.demo.exceptions.InvalidToken;
import com.example.demo.exceptions.UnauthorizedException;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.service.user.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Endpoint for creating a user
     * @param userViewDto
     * @return
     * @throws ValidationError
     */
    @PostMapping(value = "/register")
    public UserViewDto register(@RequestBody UserViewDto userViewDto) throws ValidationError {
        return this.userService.register(userViewDto);
    }

    /**
     * return a user after his token
     * @param token
     * @return
     * @throws UnauthorizedException
     * @throws InvalidToken
     */
    @GetMapping
    public UserViewDto getLoggedUser(@RequestHeader(Constants.LOOK_UP_HEADER_TOKEN) String token) throws UnauthorizedException, InvalidToken {
        return this.userService.getUserAfterToken(token);
    }
}
