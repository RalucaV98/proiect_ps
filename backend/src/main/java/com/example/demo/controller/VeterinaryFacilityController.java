package com.example.demo.controller;

import com.example.demo.dto.VeterinaryFacilityDto;
import com.example.demo.entities.VeterinaryFacility;
import com.example.demo.mapper.VeterinaryFacilityMapper;
import com.example.demo.service.veterinaryfacitlity.VeterinaryFacilityService;
import com.example.demo.service.veterinaryfacitlity.VeterinaryFacilityServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/facility")
public class VeterinaryFacilityController {

    private VeterinaryFacilityService veterinaryFacilityService;
    private VeterinaryFacilityMapper veterinaryFacilityMapper;


    public VeterinaryFacilityController(VeterinaryFacilityService veterinaryFacilityService, VeterinaryFacilityMapper veterinaryFacilityMapper) {
        this.veterinaryFacilityService = veterinaryFacilityService;
        this.veterinaryFacilityMapper = veterinaryFacilityMapper;
    }

    /**
     * Endpoint for listing all veterinary facilities stored in db
     *
     * @return List<VeterinaryFacility>
     */
    @GetMapping
    public List<VeterinaryFacilityDto> getAll() {
        return this.veterinaryFacilityService.getAll().stream()
                .map(o -> this.veterinaryFacilityMapper.entityToDto(o)).collect(Collectors.toList());
    }

}
