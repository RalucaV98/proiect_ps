package com.example.demo.dto;

import com.example.demo.entities.VeterinaryFacility;

import java.util.Date;

/**
 * Appointment data transfer object
 */
public class AppointmentDto {

    private Long id;
    private VeterinaryFacilityDto veterinaryFacility;
    private PatientDto patientDto;
    private Date date;

    public AppointmentDto() {
    }

    public AppointmentDto(Long id, VeterinaryFacilityDto veterinaryFacility, PatientDto patientDto, Date date) {
        this.id = id;
        this.veterinaryFacility = veterinaryFacility;
        this.patientDto = patientDto;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VeterinaryFacilityDto getVeterinaryFacility() {
        return veterinaryFacility;
    }

    public void setVeterinaryFacility(VeterinaryFacilityDto veterinaryFacility) {
        this.veterinaryFacility = veterinaryFacility;
    }

    public PatientDto getPatientDto() {
        return patientDto;
    }

    public void setPatientDto(PatientDto patientDto) {
        this.patientDto = patientDto;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
