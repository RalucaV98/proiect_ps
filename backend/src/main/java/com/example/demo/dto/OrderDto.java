package com.example.demo.dto;

import java.util.Date;

/**
 * Order data transfer object
 */
public class OrderDto {
    private Long id;
    private ShoppingCartDto shoppingCartDto;
    private Double price;
    private Date date;


    public OrderDto(Long id, ShoppingCartDto shoppingCartDto, Double price, Date date) {
        this.id = id;
        this.shoppingCartDto = shoppingCartDto;
        this.price = price;
        this.date = date;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShoppingCartDto getShoppingCartDto() {
        return shoppingCartDto;
    }

    public void setShoppingCartDto(ShoppingCartDto shoppingCartDto) {
        this.shoppingCartDto = shoppingCartDto;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
