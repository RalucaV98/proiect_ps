package com.example.demo.dto;

import com.example.demo.entities.Role;

/**
 * Patient data transfer object
 */
public class PatientDto extends UserViewDto {
    public PatientDto(Long id, String username, String password, Role role) {
        super(id, username, password, role);
    }

}
