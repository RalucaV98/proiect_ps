package com.example.demo.dto;

import com.fasterxml.jackson.databind.ser.Serializers;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * Product data transfer object
 */
public class ProductDto {
    private Long id;
    private String name;
    private Double price;
    private Integer quantity;
    private CategoryDto categoryDto;
    private String base64Photo;


    public ProductDto() {

    }

    public ProductDto(Long id, String name, Double price, Integer quantity, CategoryDto categoryDto) {
        this.name = name;
        this.id = id;
        this.price = price;
        this.quantity = quantity;
        this.categoryDto = categoryDto;
    }

    public ProductDto(Long id, String name, Double price, Integer quantity, CategoryDto categoryDto, byte[] photoAsBytes) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.categoryDto = categoryDto;
        try {

            this.base64Photo = Base64.getEncoder().encodeToString(photoAsBytes);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            this.base64Photo = null;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public CategoryDto getCategoryDto() {
        return categoryDto;
    }

    public void setCategoryDto(CategoryDto categoryDto) {
        this.categoryDto = categoryDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBase64Photo() {
        return base64Photo;
    }

    public void setBase64Photo(String base64Photo) {
        this.base64Photo = base64Photo;
    }
}
