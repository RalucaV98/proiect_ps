package com.example.demo.dto;

import java.util.List;

/**
 * Shopping cart data transfer object
 */
public class ShoppingCartDto {
    private Long id;
    private List<ProductDto> products;
    private Boolean active;

    public ShoppingCartDto() {
    }

    public ShoppingCartDto(Long id, List<ProductDto> products, Boolean active) {
        this.id = id;
        this.products = products;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
