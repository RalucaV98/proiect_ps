package com.example.demo.dto;

public class VeterinaryFacilityDto {

    private String name;

    private String description;

    private Double price;


    private Long id;

    public VeterinaryFacilityDto() {
    }

    public VeterinaryFacilityDto(String name, String description, Double price, Long id) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.id = id;
    }

    public VeterinaryFacilityDto(String name, String description, Double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
