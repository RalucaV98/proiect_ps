package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Admin entity.
 */
@Entity
@Table(name="admin")
public class Admin extends User {

    public Admin() {
        super();
        this.setRole(Role.ROLE_ADMIN);
    }
}
