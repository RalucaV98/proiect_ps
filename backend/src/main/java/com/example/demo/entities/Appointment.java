package com.example.demo.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Appointment entity.
 * Represent the data of an appointment stored in db
 */
@Entity
@Table(name="appointment")
public class Appointment  extends BaseEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "veterinary_facility_id")
    private VeterinaryFacility veterinaryFacility;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="patient_id")
    private Patient patient;

    @Column(name="date")
    private Date date;

    public Appointment() {
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public VeterinaryFacility getVeterinaryFacility() {
        return veterinaryFacility;
    }

    public void setVeterinaryFacility(VeterinaryFacility veterinaryService) {
        this.veterinaryFacility = veterinaryService;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
