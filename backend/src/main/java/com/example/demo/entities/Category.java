package com.example.demo.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Category entity.
 * Represent the data of a category stored in db
 */
@Entity
@Table(name = "category")
public class Category extends BaseEntity {

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy="category", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Product> productList;

    public Category() {
        super();
    }

    public Category(String name, Set<Product> productList) {
        this.name = name;
        this.productList = productList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Product> getProductList() {
        return productList;
    }

    public void setProductList(Set<Product> productList) {
        this.productList = productList;
    }
}
