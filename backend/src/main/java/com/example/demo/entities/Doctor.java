package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Doctor entity.
 * Represent the data of a doctor stored in db
 */
@Entity
@Table(name = "doctor")
public class Doctor extends User {

    public Doctor() {
        this.setRole(Role.ROLE_DOCTOR);
    }
}
