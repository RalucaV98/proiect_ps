package com.example.demo.entities;

import javax.persistence.*;
import java.util.Date;


/**
 * Order entity.
 * Represent the data of an order stored in db
 */
@Entity
@Table(name = "veterinary_order")
public class Order extends BaseEntity {

    @Column(name="total_price")
    private Double totalPrice;

    @Column(name="created_at")
    private Date date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "shopping_cart_id")
    private ShoppingCart shoppingCart;

    @Column(name = "payedBy")
    @Enumerated(EnumType.STRING)
    private PaymentType payedBy;


    public Order() {
        super();
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public PaymentType getPayedBy() {
        return payedBy;
    }

    public void setPayedBy(PaymentType payedBy) {
        this.payedBy = payedBy;
    }
}
