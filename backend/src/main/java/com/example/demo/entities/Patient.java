package com.example.demo.entities;


import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Patient entity.
 * Represent the data of a patient stored in db
 */
@Entity
@Table(name="patient")
public class Patient extends User {

    public Patient() {
        super();
        this.setRole(Role.ROLE_PATIENT);
    }
}
