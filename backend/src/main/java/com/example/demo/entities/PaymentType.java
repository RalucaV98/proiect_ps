package com.example.demo.entities;

public enum PaymentType {
    PayPal,
    Card
}
