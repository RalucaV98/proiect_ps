package com.example.demo.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Prodcuct entity.
 * Represent the data of a product stored in db
 */
@Entity
@Table(name = "product")
public class Product extends BaseEntity {

    private String name;
    private Double price;
    private Integer quantity;

    @ManyToMany(mappedBy = "products", cascade = CascadeType.REMOVE)
    private List<ShoppingCart> carts = new ArrayList<>();

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    @Lob
    @Column(name = "photo", columnDefinition="BLOB")
    private byte[] photo;

    public Product() {
    }

    public Product(String name, Double price, Integer quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public Product(String name, Double price, Integer quantity, List<ShoppingCart> carts, Category category, byte[] photo) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.carts = carts;
        this.category = category;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public List<ShoppingCart> getCarts() {
        return carts;
    }

    public void setCarts(List<ShoppingCart> carts) {
        this.carts = carts;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }
}
