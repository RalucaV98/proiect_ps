package com.example.demo.entities;


import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Role entity.
 * Represent the data of a role stored in db
 */
public enum Role {
    ROLE_PATIENT,
    ROLE_DOCTOR,
    ROLE_ADMIN
}
