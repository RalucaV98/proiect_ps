package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * VeterinaryFacility entity.
 */
@Entity
@Table(name = "veterinary_facility")
public class VeterinaryFacility extends BaseEntity {

    private String name;

    private String description;

    private Double price;

    public VeterinaryFacility() {
        super();
    }

    public VeterinaryFacility(String name, String description, Double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
