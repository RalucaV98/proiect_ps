package com.example.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid image type")
public class InvalidMediaType extends Exception {
}
