package com.example.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN)
public class UnauthorizedException extends Exception {
    public UnauthorizedException(String s) {
        super(s);
    }

    public UnauthorizedException() {
        super("Unauthorized");
    }
}
