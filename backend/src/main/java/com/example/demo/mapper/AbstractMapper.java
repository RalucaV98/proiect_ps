package com.example.demo.mapper;

public abstract class AbstractMapper<T, V> {
    public abstract T dtoToEntity(V o);
    public abstract V entityToDto(T o);
}
