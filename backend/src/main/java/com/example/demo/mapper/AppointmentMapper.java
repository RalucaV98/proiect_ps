package com.example.demo.mapper;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.dto.VeterinaryFacilityDto;
import com.example.demo.entities.Appointment;
import org.springframework.stereotype.Component;

/**
 * Mapper class between Appointment and AppointmentDto
 * This class converts Appointment entity to dto and back
 */
@Component
public class AppointmentMapper extends AbstractMapper<Appointment, AppointmentDto> {
    private PatientMapper patientMapper;
    private VeterinaryFacilityMapper veterinaryFacilityMapper;


    public AppointmentMapper(PatientMapper patientMapper, VeterinaryFacilityMapper veterinaryFacilityMapper) {
        this.patientMapper = patientMapper;
        this.veterinaryFacilityMapper = veterinaryFacilityMapper;
    }

    @Override
    public Appointment dtoToEntity(AppointmentDto o) {
        return null;
    }

    /**
     * Convert appointment to appointment dto
     * @param o
     * @return
     */
    @Override
    public AppointmentDto entityToDto(Appointment o) {
        return new AppointmentDto(
                o.getId(),
                this.veterinaryFacilityMapper.entityToDto(o.getVeterinaryFacility()),
                this.patientMapper.entityToDto(o.getPatient()),
                o.getDate()
        );
    }
}
