package com.example.demo.mapper;

import com.example.demo.dto.CategoryDto;
import com.example.demo.entities.Category;
import org.springframework.stereotype.Component;

import java.util.HashSet;

/**
 * Mapper class between Category and CategoryDto
 * This class converts Category entity to dto and back
 */
@Component
public class CategoryMapper extends AbstractMapper<Category, CategoryDto> {

    @Override
    public Category dtoToEntity(CategoryDto o) {
        Category category = new Category();
        category.setId(o.getId());
        category.setName(o.getName());

        return category;
    }

    /**
     * Convert Category entity to dto
     * @param o Category
     * @return ProductDto
     */
    @Override
    public CategoryDto entityToDto(Category o) {
        return new CategoryDto(o.getId(), o.getName());
    }
}
