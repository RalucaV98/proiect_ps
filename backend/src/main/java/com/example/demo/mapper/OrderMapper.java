package com.example.demo.mapper;

import com.example.demo.dto.OrderDto;
import com.example.demo.entities.Order;
import org.springframework.stereotype.Component;

/**
 * Order mapper
 * Converts Order entity to dto and back
 */
@Component
public class OrderMapper extends AbstractMapper<Order, OrderDto> {

    private ShoppingCartMapper shoppingCartMapper;

    public OrderMapper(ShoppingCartMapper shoppingCartMapper) {
        this.shoppingCartMapper = shoppingCartMapper;
    }

    @Override
    public Order dtoToEntity(OrderDto o) {
        return null;
    }

    @Override
    public OrderDto entityToDto(Order o) {
        return new OrderDto(
                o.getId(),
                this.shoppingCartMapper.entityToDto(o.getShoppingCart()),
                o.getTotalPrice(),
                o.getDate()
                );
    }
}
