package com.example.demo.mapper;

import com.example.demo.dto.PatientDto;
import com.example.demo.entities.Patient;
import org.springframework.stereotype.Component;

@Component
public class PatientMapper extends AbstractMapper<Patient, PatientDto> {
    @Override
    public PatientDto entityToDto(Patient o) {
        return new PatientDto(o.getId(), o.getUsername(), o.getPassword(), o.getRole());
    }

    @Override
    public Patient dtoToEntity(PatientDto o) {
        return null;
    }
}
