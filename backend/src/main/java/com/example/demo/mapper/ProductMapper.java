package com.example.demo.mapper;

import com.example.demo.dto.ProductDto;
import com.example.demo.entities.Category;
import com.example.demo.entities.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Base64;

/**
 * Mapper class between Product and ProductDto
 * This class converts Product entity to dto and back
 */
@Component
public class ProductMapper extends AbstractMapper<Product, ProductDto> {

    private CategoryMapper categoryMapper;

    public ProductMapper(CategoryMapper categoryMapper) {
        this.categoryMapper = categoryMapper;
    }

    /**
     * Convert product dto to entity
     * @param o ProductDto
     * @return Product
     */
    @Override
    public Product dtoToEntity(ProductDto o) {
        Product product = new Product();

        if (o.getId() != null) {
            product.setId(o.getId());
        }

        product.setCategory(this.categoryMapper.dtoToEntity(o.getCategoryDto()));
        product.setCarts(new ArrayList<>());
        product.setName(o.getName());
        product.setPrice(o.getPrice());
        product.setQuantity(o.getQuantity());
        product.setPhoto(Base64.getDecoder().decode(o.getBase64Photo()));

        return product;
    }

    /**
     * Convert Product entity to dto
     * @param o Product
     * @return ProductDto
     */
    @Override
    public ProductDto entityToDto(Product o) {
        return new ProductDto(
                o.getId(),
                o.getName(),
                o.getPrice(),
                o.getQuantity(),
                this.categoryMapper.entityToDto(o.getCategory()),
                o.getPhoto()
        );
    }
}
