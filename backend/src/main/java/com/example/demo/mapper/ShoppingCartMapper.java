package com.example.demo.mapper;

import com.example.demo.dto.ShoppingCartDto;
import com.example.demo.entities.ShoppingCart;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Shopping cart mapper
 * Convert shoopping cart entity to shoppingcartdto and back
 */
@Component
public class ShoppingCartMapper extends AbstractMapper<ShoppingCart, ShoppingCartDto> {

    private ProductMapper productMapper;

    public ShoppingCartMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Override
    public ShoppingCart dtoToEntity(ShoppingCartDto o) {
        return null;
    }

    @Override
    public ShoppingCartDto entityToDto(ShoppingCart o) {
        return new ShoppingCartDto(o.getId(),
                o.getProducts().stream().map(p -> this.productMapper.entityToDto(p)).collect(Collectors.toList()),
                o.getActive()
                );
    }
}
