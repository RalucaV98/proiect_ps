package com.example.demo.mapper;

import com.example.demo.auth.Token;
import com.example.demo.dto.TokenDto;
import org.springframework.stereotype.Component;

/**
 * Convert Token instance to TokenDto and back
 */
@Component
public class TokenMapper extends AbstractMapper<Token, TokenDto> {

    @Override
    public Token dtoToEntity(TokenDto o) {
        return null;
    }

    @Override
    public TokenDto entityToDto(Token o) {
        return new TokenDto(o.getValue());
    }
}
