package com.example.demo.mapper;

import com.example.demo.dto.UserViewDto;
import com.example.demo.entities.User;
import org.springframework.stereotype.Component;

/**
 * User mapper
 * Converts User entity to UserViewDto and back
 */
@Component
public class UserViewMapper extends AbstractMapper<User, UserViewDto> {
    @Override
    public User dtoToEntity(UserViewDto o) {
        return null;
    }

    @Override
    public UserViewDto entityToDto(User o) {
        return new UserViewDto(o.getId(), o.getUsername(), o.getPassword(), o.getRole());
    }
}
