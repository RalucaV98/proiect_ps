package com.example.demo.mapper;

import com.example.demo.dto.VeterinaryFacilityDto;
import com.example.demo.entities.VeterinaryFacility;
import org.springframework.stereotype.Component;

@Component
public class VeterinaryFacilityMapper extends AbstractMapper<VeterinaryFacility, VeterinaryFacilityDto> {

    @Override
    public VeterinaryFacility dtoToEntity(VeterinaryFacilityDto o) {
        return null;
    }

    @Override
    public VeterinaryFacilityDto entityToDto(VeterinaryFacility o) {
        return new VeterinaryFacilityDto(o.getName(), o.getDescription(), o.getPrice(), o.getId());
    }
}
