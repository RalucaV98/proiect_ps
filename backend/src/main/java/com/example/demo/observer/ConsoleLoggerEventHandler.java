package com.example.demo.observer;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


/**
 * Concrete observer
 */
@Component
public class ConsoleLoggerEventHandler implements ApplicationListener<MessageEvent> {

    @Override
    public void onApplicationEvent(MessageEvent messageEvent) {
        System.out.println("ConsoleLogger: " + messageEvent);
    }
}
