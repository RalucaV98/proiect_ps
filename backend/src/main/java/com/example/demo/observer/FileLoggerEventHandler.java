package com.example.demo.observer;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Concrete observer
 */
@Component
public class FileLoggerEventHandler implements ApplicationListener<MessageEvent> {

    @Override
    public void onApplicationEvent(MessageEvent messageEvent) {
        System.out.println("FileLogger:" + messageEvent); // TODO: 29.03.2020: write logs into a file
    }
}
