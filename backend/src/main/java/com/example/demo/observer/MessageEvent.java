package com.example.demo.observer;

import org.springframework.context.ApplicationEvent;

public class MessageEvent extends ApplicationEvent {

    private String message;

    public MessageEvent(Object source, String message) {
        super(source);
        this.message = message;
    }


    @Override
    public String toString() {
        return "MessageEvent [message="+ this.message + "]";
    }
}
