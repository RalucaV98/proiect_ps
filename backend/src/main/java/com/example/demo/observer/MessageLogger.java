package com.example.demo.observer;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * Subject
 * Notify all observers when writeLog method is called
 */
@Component
public class MessageLogger implements ApplicationEventPublisherAware {
    private ApplicationEventPublisher applicationEventPublisher = null;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void writeLog(String message) {
        this.applicationEventPublisher.publishEvent(new MessageEvent(this, message));
    }
}
