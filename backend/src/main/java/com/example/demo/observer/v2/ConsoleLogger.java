package com.example.demo.observer.v2;

import org.springframework.stereotype.Component;

/**
 * Concrete observer
 */
@Component
public class ConsoleLogger extends Observer {

    public ConsoleLogger(MessageLoggerV2 messageLoggerV2) {
        this.messageLoggerV2 = messageLoggerV2;
        this.messageLoggerV2.attach(this);
    }

    @Override
    public void update(String msg) {
        System.out.println("ConsoleLogger v2: " + msg);
    }
}
