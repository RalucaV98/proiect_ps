package com.example.demo.observer.v2;

/**
 * Demo class for testing Observer DP
 */
public class Demo {
    public static void main(String[] args) {
        MessageLoggerV2 messageLoggerV2 = new MessageLoggerV2();
        ConsoleLogger consoleLogger = new ConsoleLogger(messageLoggerV2);
        FileLogger fileLogger = new FileLogger(messageLoggerV2);

        messageLoggerV2.log("test");

    }
}
