package com.example.demo.observer.v2;

import org.springframework.stereotype.Component;

/**
 * Concrete observer
 */
@Component
public class FileLogger extends Observer {

    public FileLogger(MessageLoggerV2 messageLoggerV2) {
        this.messageLoggerV2 = messageLoggerV2;
        this.messageLoggerV2.attach(this);
    }

    @Override
    public void update(String msg) {
        System.out.println("FileLogger v2: " + msg);
    }
}
