package com.example.demo.observer.v2;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Subject
 */
@Component
public class MessageLoggerV2 {
    public List<Observer> observerList = new ArrayList<>();

    public void attach(Observer observer) {
        this.observerList.add(observer);
    }

    public void detach(Observer observer) {
        this.observerList.remove(observer);
    }

    public void log(String msg) {
        for (Observer o: this.observerList) {
            o.update(msg);
        }
    }
}
