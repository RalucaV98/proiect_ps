package com.example.demo.observer.v2;

/**
 * Abstract observer
 */
public abstract class Observer {
    protected MessageLoggerV2 messageLoggerV2;

    abstract void update(String msg);
}
