package com.example.demo.payment;

import com.example.demo.entities.Order;
import com.example.demo.entities.PaymentType;
import com.example.demo.exceptions.PaymentFailed;

/**
 * Credit card payment strategy
 */
public class CreditCardStrategy implements PaymentStrategy {

    private String name;
    private String cardNumber;
    private String cvv;
    private String dateOfExpiry;

    public CreditCardStrategy() {
    }

    public CreditCardStrategy(String name, String cardNumber, String cvv, String dateOfExpiry) {
        this.name = name;
        this.cardNumber = cardNumber;
        this.cvv = cvv;
        this.dateOfExpiry = dateOfExpiry;
    }

    @Override
    public void pay(Order order) throws PaymentFailed {
        // check if card is valid.
        if (this.name.equals("") || this.cardNumber.equals("") || this.cvv.equals("") || this.dateOfExpiry.equals("")) {
            throw new PaymentFailed();
        }

        order.setPayedBy(PaymentType.Card);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(String dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }
}
