package com.example.demo.payment;

import com.example.demo.entities.Order;
import com.example.demo.entities.PaymentType;
import com.example.demo.exceptions.PaymentFailed;

/**
 * Pay Pal payment strategy
 */
public class PayPalStrategy implements PaymentStrategy {

    private String email;
    private String password;

    public PayPalStrategy() {

    }

    public PayPalStrategy(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public void pay(Order order) throws PaymentFailed {
        if (this.email.equals("") || this.password.equals("")) {
            throw new PaymentFailed();
        }

        order.setPayedBy(PaymentType.PayPal);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
