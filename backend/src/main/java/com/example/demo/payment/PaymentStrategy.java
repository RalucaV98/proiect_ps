package com.example.demo.payment;


import com.example.demo.entities.Order;
import com.example.demo.exceptions.PaymentFailed;

public interface PaymentStrategy {
    public void pay(Order order) throws PaymentFailed;
}
