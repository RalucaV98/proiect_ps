package com.example.demo.report;

public interface Report {
    String generateReport();
}
