package com.example.demo.report;


import org.springframework.stereotype.Component;

@Component
public class ReportFacade {

    private TxtReport txtReport;
    private XlsReport xlsReport;

    public ReportFacade(TxtReport txtReport, XlsReport xlsReport) {
        this.txtReport = txtReport;
        this.xlsReport = xlsReport;
    }


    public String generateTxtReport() {
        return this.txtReport.generateReport();
    }

    public String generateXlxReport() {
        return this.xlsReport.generateReport();
    }
}
