package com.example.demo.report;

import org.springframework.stereotype.Component;

@Component
public class ReportFactory {
    private TxtReport txtReport;
    private XlsReport xlsReport;

    public ReportFactory(TxtReport txtReport, XlsReport xlsReport) {
        this.txtReport = txtReport;
        this.xlsReport = xlsReport;
    }

    /**
     * get object of type report
     */
    public Report mostSoldProductsReport(ReportType reportType) {
        if (reportType == ReportType.TXT) {
            return this.txtReport;
        }

        return this.xlsReport;
    }
}
