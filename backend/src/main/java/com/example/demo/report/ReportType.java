package com.example.demo.report;

/**
 * the types of files in which a report can be generated
 */
public enum ReportType {
    TXT,
    XLS
}
