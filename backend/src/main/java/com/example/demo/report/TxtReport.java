package com.example.demo.report;

import com.example.demo.entities.Product;
import com.example.demo.service.order.OrderService;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.util.Map;

@Component
public class TxtReport implements Report {

    private OrderService orderService;

    public TxtReport(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    /**
     *Generate a top most sold products report in .txt file
     */
    public String generateReport() {
        Map<Product, Long> mostSoldProducts = this.orderService.getMostSoldProducts(5);
        String fname = "report.txt";

        try {
            FileWriter writer = new FileWriter(fname);

            for (Map.Entry<Product, Long> p: mostSoldProducts.entrySet()) {
                writer.write(p.getKey().toString() + " : " + p.getValue() + "\n");
            }
            writer.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return fname;
    }
}
