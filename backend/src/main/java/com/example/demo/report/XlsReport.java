package com.example.demo.report;

import com.example.demo.entities.Product;
import com.example.demo.service.order.OrderService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.util.Map;

@Component
public class XlsReport implements Report {

    private OrderService orderService;

    public XlsReport(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    /**
     *Generate a top most sold products report in .xsls file; return the name of the file
     */
    public String generateReport() {
        Map<Product, Long> mostSoldProducts = this.orderService.getMostSoldProducts(5);

        String fname = "report.xlsx";

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Most sold products");

        int rowCount = 0;

        for (Map.Entry<Product, Long> productsCount : mostSoldProducts.entrySet()) {
            Row row = sheet.createRow(++rowCount);

            Cell cell1 = row.createCell(1);
            Cell cell2 = row.createCell(2);

            cell1.setCellValue(productsCount.getKey().getName());
            cell2.setCellValue(productsCount.getValue());
        }

        try (FileOutputStream outputStream = new FileOutputStream(fname)) {
            workbook.write(outputStream);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return fname;
    }
}
