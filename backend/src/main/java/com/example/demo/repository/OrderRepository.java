package com.example.demo.repository;

import com.example.demo.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Order data access object
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
