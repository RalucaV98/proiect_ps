package com.example.demo.repository;

import com.example.demo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Patient data access object
 */
@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
}
