package com.example.demo.repository;

import com.example.demo.entities.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long> {
    List<ShoppingCart> getAllByPatient_IdAndActive(Long patientId, Boolean active);
}
