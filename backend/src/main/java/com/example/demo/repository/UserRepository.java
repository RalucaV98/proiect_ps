package com.example.demo.repository;

import com.example.demo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * User data access object
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Select * from user where username = ?username and password = ?password
     * @param username
     * @param password
     * @return
     */
    User getUserByUsernameAndPassword(String username, String password);
    User getUserByUsername(String username);
}
