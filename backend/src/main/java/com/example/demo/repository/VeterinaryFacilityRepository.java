package com.example.demo.repository;

import com.example.demo.entities.VeterinaryFacility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * VeterinaryFacility data access object
 */
@Repository
public interface VeterinaryFacilityRepository extends JpaRepository<VeterinaryFacility, Long> {
}
