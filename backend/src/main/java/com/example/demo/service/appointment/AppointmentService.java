package com.example.demo.service.appointment;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.exceptions.ValidationError;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AppointmentService {
    List<AppointmentDto> getAll();
    List<AppointmentDto> getAllWhereDateIsGreaterThanToday();
    AppointmentDto createAppointment(AppointmentDto appointmentDto) throws ValidationError;
}
