package com.example.demo.service.appointment;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.entities.Appointment;
import com.example.demo.entities.Patient;
import com.example.demo.entities.VeterinaryFacility;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.mapper.AppointmentMapper;
import com.example.demo.repository.AppointmentRepository;
import com.example.demo.repository.PatientRepository;
import com.example.demo.repository.VeterinaryFacilityRepository;
import com.sun.xml.fastinfoset.util.ValueArrayResourceException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private AppointmentMapper appointmentMapper;
    private AppointmentRepository appointmentRepository;
    private PatientRepository patientRepository;
    private VeterinaryFacilityRepository veterinaryFacilityRepository;

    public AppointmentServiceImpl(AppointmentMapper appointmentMapper, AppointmentRepository appointmentRepository,
                                  PatientRepository patientRepository, VeterinaryFacilityRepository veterinaryFacilityRepository) {
        this.appointmentMapper = appointmentMapper;
        this.appointmentRepository = appointmentRepository;
        this.patientRepository = patientRepository;
        this.veterinaryFacilityRepository = veterinaryFacilityRepository;
    }


    /**
     * Get all the appointments stored in database
     * @return
     */
    @Override
    public List<AppointmentDto> getAll() {
        return this.appointmentRepository.findAll().stream()
                .map(a -> this.appointmentMapper.entityToDto(a))
                .collect(Collectors.toList());
    }

    /**
     * Get all the appointments from the 'future'
     * @return
     */
    @Override
    public List<AppointmentDto> getAllWhereDateIsGreaterThanToday() {
        return this.appointmentRepository.getAllByDateIsAfter(new Date()).stream()
                .map(a -> this.appointmentMapper.entityToDto(a))
                .collect(Collectors.toList());
    }

    /**
     * Create an appointment
     * @return AppointmentDto
     */
    @Override
    public AppointmentDto createAppointment(AppointmentDto appointmentDto) throws ValidationError {
        Patient patient = this.patientRepository.getOne(appointmentDto.getPatientDto().getId());
        VeterinaryFacility veterinaryFacility = this.veterinaryFacilityRepository.getOne(appointmentDto.getVeterinaryFacility().getId());


        if (patient.getId() == null) {
            throw new ValidationError("Patient not found");
        }

        if (veterinaryFacility.getId() == null) {
            throw new ValidationError("Veterinary service not found");
        }

        if (appointmentDto.getDate().before(new Date())) {
            throw new ValidationError("You can not create an appointment in the past");
        }

        Appointment appointment = new Appointment();
        appointment.setDate(appointmentDto.getDate());
        appointment.setPatient(patient);
        appointment.setVeterinaryFacility(veterinaryFacility);

        return this.appointmentMapper.entityToDto(this.appointmentRepository.save(appointment));
    }
}
