package com.example.demo.service.category;

import com.example.demo.dto.CategoryDto;
import com.example.demo.entities.Category;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {
    List<Category> getAllCategories();
    Category save(CategoryDto categoryDto);
    Category getAfterId(Long id);

    void deleteById(Long id);
}
