package com.example.demo.service.category;

import com.example.demo.dto.CategoryDto;
import com.example.demo.entities.Category;
import com.example.demo.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    /**
     * @return a list with all categories from db
     */
    @Override
    public List<Category> getAllCategories() {
        return this.categoryRepository.findAll();
    }

    /**
     * Create a new category
     * @param categoryDto
     * @return created category
     */
    @Override
    public Category save(CategoryDto categoryDto) {
        Category category = new Category();

        try {
            if (categoryDto.getId() != null) {
                category.setId(categoryDto.getId());
            }
        } catch (Exception ex) {

        }

        category.setProductList(new HashSet<>());
        category.setName(categoryDto.getName());

        return this.categoryRepository.save(category);
    }

    /**
     * Retrieve a category after id
     * @param id
     * @return
     */
    @Override
    public Category getAfterId(Long id) {
        return this.categoryRepository.getOne(id);
    }

    /**
     * Delete a category from db after id
     * @param id Long
     */
    @Override
    public void deleteById(Long id) {
        this.categoryRepository.deleteById(id);
    }
}
