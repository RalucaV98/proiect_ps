package com.example.demo.service.filestorage;

import com.example.demo.entities.Product;
import com.example.demo.exceptions.InvalidMediaType;
import com.example.demo.exceptions.ValidationError;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public interface ImageService {
    Product updateProductImage(MultipartFile file, Long productId) throws InvalidMediaType, ValidationError, IOException;
}
