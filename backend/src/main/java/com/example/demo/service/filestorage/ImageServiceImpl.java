package com.example.demo.service.filestorage;

import com.example.demo.entities.Product;
import com.example.demo.exceptions.InvalidMediaType;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.repository.ProductRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService {

    private ProductRepository productRepository;

    public ImageServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Update product image. Image is saved as blob
     * @param file The image file
     * @param productId The id of the product
     * @return The updated product
     * @throws InvalidMediaType
     * @throws ValidationError
     * @throws IOException
     */
    public Product updateProductImage(MultipartFile file, Long productId) throws InvalidMediaType, ValidationError, IOException {
        // check extension
        String ext = file.getOriginalFilename().split("\\.")[1];

        if (!(ext.equals("jpg") || ext.equals("png"))) {
            throw new InvalidMediaType();
        }

        Product product = null;
        try {
            product = this.productRepository.getOne(productId);
            product.getId();
        } catch (Exception e) {
            throw new ValidationError("Product not found");
        }


        product.setPhoto(file.getBytes());

        return this.productRepository.save(product);
    }
}
