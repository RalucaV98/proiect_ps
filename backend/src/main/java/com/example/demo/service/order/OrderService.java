package com.example.demo.service.order;

import com.example.demo.dto.OrderDto;
import com.example.demo.entities.Product;
import com.example.demo.exceptions.PaymentFailed;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.payment.PaymentStrategy;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface OrderService {
    Map<Product, Long> getMostSoldProducts(Integer limit);
    OrderDto placeOrder(Long shoppingCartId) throws ValidationError;
    PaymentStrategy payOrder(PaymentStrategy paymentStrategy, Long orderId) throws ValidationError, PaymentFailed;
}
