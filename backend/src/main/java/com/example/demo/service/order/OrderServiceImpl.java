package com.example.demo.service.order;

import com.example.demo.dto.OrderDto;
import com.example.demo.entities.Order;
import com.example.demo.entities.Product;
import com.example.demo.entities.ShoppingCart;
import com.example.demo.exceptions.PaymentFailed;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.payment.PaymentStrategy;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.ShoppingCartRepository;
import com.example.demo.util.DefaultHashMap;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private ShoppingCartRepository shoppingCartRepository;
    private OrderMapper orderMapper;

    public OrderServiceImpl(OrderRepository orderRepository,
                            ShoppingCartRepository shoppingCartRepository,
                            OrderMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.shoppingCartRepository = shoppingCartRepository;
        this.orderMapper = orderMapper;
    }

    /**
     * Calculate how many times a product is sold
     *
     * @param limit Top 'limit' products
     * @return Map<Product, Long>
     */
    public Map<Product, Long> getMostSoldProducts(Integer limit) {
        List<Order> orders = this.orderRepository.findAll();

        Map<Product, Long> productsCount = new DefaultHashMap<Product, Long>(0L);

        for (Order o : orders) {
            for (Product p : o.getShoppingCart().getProducts()) {
                productsCount.put(p, productsCount.get(p) + 1);
            }
        }

        return
                productsCount.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .limit(limit)
                        .collect(Collectors.toMap(
                                Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    /**
     * Create an order instance with the given shopping cart
     * After placing the order shopping cart will be disable
     *
     * @param shoppingCartId
     * @return OrderDto
     */
    @Override
    public OrderDto placeOrder(Long shoppingCartId) throws ValidationError {
        ShoppingCart shoppingCart = this.shoppingCartRepository.getOne(shoppingCartId);

        if (shoppingCart.getId() == null) {
            throw new ValidationError("Shopping cart not found");
        }

        if (!shoppingCart.getActive()) {
            throw new ValidationError("Shopping cart is not active");
        }

        if(shoppingCart.getProducts().size() == 0) {
            throw new ValidationError("Shopping cart is empty");
        }


        // compute price

        Double totalPrice = 0.0d;
        for (Product p: shoppingCart.getProducts()) {
            totalPrice += p.getPrice();
        }

        Order order = new Order();
        order.setShoppingCart(shoppingCart);
        order.setDate(new Date());
        order.setTotalPrice(totalPrice);
        shoppingCart.setActive(false);
        this.shoppingCartRepository.save(shoppingCart);

        return this.orderMapper.entityToDto(this.orderRepository.save(order));
    }

    /**
     * Pay order functionality
     * @param paymentStrategy
     * @param orderId
     * @return
     * @throws ValidationError
     * @throws PaymentFailed
     */
    @Override
    public PaymentStrategy payOrder(PaymentStrategy paymentStrategy, Long orderId) throws ValidationError, PaymentFailed {
        Order order = this.orderRepository.getOne(orderId);
        if (order.getId() == null) {
            throw new ValidationError("Order not found");
        }

        paymentStrategy.pay(order);

        this.orderRepository.save(order);

        return paymentStrategy;
    }
}
