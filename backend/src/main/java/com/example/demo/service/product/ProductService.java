package com.example.demo.service.product;

import com.example.demo.dto.ProductDto;
import com.example.demo.entities.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<Product> getAll();
    Product getById(Long id);
    void deleteById(Long id);
    Product saveProduct(ProductDto productDto);
}
