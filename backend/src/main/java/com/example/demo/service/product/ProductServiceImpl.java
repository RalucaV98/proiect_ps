package com.example.demo.service.product;

import com.example.demo.dto.ProductDto;
import com.example.demo.entities.Product;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    /**
     * Get all products from db
     * @return List<Product>
     */
    @Override
    public List<Product> getAll() {
        return this.productRepository.findAll();
    }

    /**
     * Retrieve a product after id
     * @param id
     * @return
     */
    @Override
    public Product getById(Long id) {
        return this.productRepository.getOne(id);
    }

    /**
     * Delete a product after id
     * @param id Long
     */
    @Override
    public void deleteById(Long id) {
        this.productRepository.deleteById(id);
    }

    /**
     * Update/Create a product
     * @param productDto
     * @return Product
     */
    @Override
    public Product saveProduct(ProductDto productDto) {
        Product product = this.productMapper.dtoToEntity(productDto);

        return this.productRepository.save(product);
    }
}
