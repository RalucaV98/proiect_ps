package com.example.demo.service.shoppingcart;

import com.example.demo.dto.ProductDto;
import com.example.demo.dto.ShoppingCartDto;
import com.example.demo.entities.ShoppingCart;
import com.example.demo.exceptions.InvalidToken;
import com.example.demo.exceptions.UnauthorizedException;
import com.example.demo.exceptions.ValidationError;
import org.springframework.stereotype.Service;

@Service
public interface ShoppingCartService {
     ShoppingCartDto getUserShoppingCart(String token) throws UnauthorizedException, InvalidToken;
     ShoppingCartDto addProductToCart(Long shoppingCartId, Long productId) throws ValidationError;

    ShoppingCartDto removeProductFromCart(Long shoppingCartId, Long productId) throws ValidationError;
}
