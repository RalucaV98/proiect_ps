package com.example.demo.service.shoppingcart;

import com.example.demo.auth.SecurityContextHolder;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.ShoppingCartDto;
import com.example.demo.entities.*;
import com.example.demo.exceptions.InvalidToken;
import com.example.demo.exceptions.UnauthorizedException;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.mapper.ShoppingCartMapper;
import com.example.demo.repository.PatientRepository;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ShoppingCartRepository;
import org.springframework.stereotype.Service;
import sun.awt.image.ShortInterleavedRaster;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {


    private ShoppingCartRepository shoppingCartRepository;
    private PatientRepository patientRepository;
    private SecurityContextHolder securityContextHolder;
    private ShoppingCartMapper shoppingCartMapper;
    private ProductRepository productRepository;


    public ShoppingCartServiceImpl(ShoppingCartRepository shoppingCartRepository,
                                   PatientRepository patientRepository,
                                   SecurityContextHolder securityContextHolder,
                                   ShoppingCartMapper shoppingCartMapper,
                                   ProductRepository productRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.patientRepository = patientRepository;
        this.securityContextHolder = securityContextHolder;
        this.shoppingCartMapper = shoppingCartMapper;
        this.productRepository = productRepository;
    }


    /**
     * Get user active cart
     * Only one shopping cart must be active at a specific time for a user
     *
     * @param token
     * @return
     * @throws UnauthorizedException
     * @throws InvalidToken
     */
    @Override
    public ShoppingCartDto getUserShoppingCart(String token) throws UnauthorizedException, InvalidToken {
        User user = this.securityContextHolder.getUserAfterToken(token);

        if (!user.getRole().equals(Role.ROLE_PATIENT)) {
            throw new UnauthorizedException("You don't have permission");
        }

        List<ShoppingCart> shoppingCartList = this.shoppingCartRepository.getAllByPatient_IdAndActive(user.getId(), true);

        if (shoppingCartList.size() == 0) {
            Patient patient = this.patientRepository.getOne(user.getId());
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.setActive(true);
            shoppingCart.setProducts(new ArrayList<>());
            shoppingCart.setPatient(patient);

            return this.shoppingCartMapper.entityToDto(this.shoppingCartRepository.save(shoppingCart));
        }

        return this.shoppingCartMapper.entityToDto(shoppingCartList.get(0));
    }

    /**
     * Add product to cart
     *
     * @param shoppingCartId The id of the shopping cart
     * @param productId      The id of the product that will be added in the cart
     * @return ShoppingCartDto
     * @throws ValidationError
     */
    @Override
    public ShoppingCartDto addProductToCart(Long shoppingCartId, Long productId) throws ValidationError {

        ShoppingCart shoppingCart = this.shoppingCartRepository.getOne(shoppingCartId);
        Product product = this.productRepository.getOne(productId);

        if (shoppingCart.getId() == null) {
            throw new ValidationError("Shopping cart not found");
        }

        if (product.getId() == null) {
            throw new ValidationError("Product not found");
        }

        shoppingCart.getProducts().add(product);
        return this.shoppingCartMapper.entityToDto(this.shoppingCartRepository.save(shoppingCart));
    }

    /**
     * Remove a product from a shopping cart
     *
     * @param shoppingCartId The id of the shopping cart
     * @param productId      The id of the product
     * @return ShoppingCartDto
     * @throws ValidationError
     */
    @Override
    public ShoppingCartDto removeProductFromCart(Long shoppingCartId, Long productId) throws ValidationError {
        ShoppingCart shoppingCart = this.shoppingCartRepository.getOne(shoppingCartId);
        Product product = this.productRepository.getOne(productId);

        if (shoppingCart.getId() == null) {
            throw new ValidationError("Shopping cart not found");
        }

        if (product.getId() == null) {
            throw new ValidationError("Product not found");
        }

        if (shoppingCart.getProducts().size() == 0) {
            throw new ValidationError("Your shopping cart is empty");
        }

        List<Product> products = new ArrayList<>();

        for (Product p : shoppingCart.getProducts()) {
            if (!p.getId().equals(product.getId())) {
                products.add(p);
            }
        }

        if (products.size() == shoppingCart.getProducts().size()) {
            throw new ValidationError("Product not found in the cart");
        }

        shoppingCart.setProducts(products);

        return this.shoppingCartMapper.entityToDto(this.shoppingCartRepository.save(shoppingCart));
    }
}
