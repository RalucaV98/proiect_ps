package com.example.demo.service.user;

import com.example.demo.auth.Token;
import com.example.demo.dto.TokenDto;
import com.example.demo.dto.UserViewDto;
import com.example.demo.exceptions.InvalidCredentials;
import com.example.demo.exceptions.InvalidToken;
import com.example.demo.exceptions.UnauthorizedException;
import com.example.demo.exceptions.ValidationError;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    TokenDto logIn(String username, String password) throws InvalidCredentials;
    void logOut(String token) throws InvalidToken;

    UserViewDto getUserAfterToken(String token) throws UnauthorizedException, InvalidToken;

    UserViewDto register(UserViewDto userViewDto) throws ValidationError;
}
