package com.example.demo.service.user;

import com.example.demo.auth.SecurityContextHolder;
import com.example.demo.dto.TokenDto;
import com.example.demo.dto.UserViewDto;
import com.example.demo.entities.*;
import com.example.demo.exceptions.InvalidCredentials;
import com.example.demo.exceptions.InvalidToken;
import com.example.demo.exceptions.UnauthorizedException;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.mapper.UserViewMapper;
import com.example.demo.repository.AdminRepository;
import com.example.demo.repository.DoctorRepository;
import com.example.demo.repository.PatientRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private SecurityContextHolder securityContextHolder;
    private UserViewMapper userViewMapper;
    private AdminRepository adminRepository;
    private DoctorRepository doctorRepository;
    private PatientRepository patientRepository;


    public UserServiceImpl(UserRepository userRepository, SecurityContextHolder securityContextHolder,
                           UserViewMapper userViewMapper, AdminRepository adminRepository,
                           DoctorRepository doctorRepository, PatientRepository patientRepository
    ) {
        this.userRepository = userRepository;
        this.securityContextHolder = securityContextHolder;
        this.userViewMapper = userViewMapper;
        this.adminRepository = adminRepository;
        this.doctorRepository = doctorRepository;
        this.patientRepository = patientRepository;
    }

    /**
     * Login functionality
     * Check if a exist a user after the given credentials. It creates a random token if the credentials are valid
     * @param username
     * @param password
     * @return
     * @throws InvalidCredentials
     */
    @Override
    public TokenDto logIn(String username, String password) throws InvalidCredentials {
        User user = this.userRepository.getUserByUsernameAndPassword(username, password);

        if (user == null) {
            throw new InvalidCredentials();
        }

        return new TokenDto(this.securityContextHolder.generateAndAddToken(user));
    }

    /**
     * Remove Token from SecurityContextHolder
     * @param token
     * @throws InvalidToken
     */
    @Override
    public void logOut(String token) throws InvalidToken {

        this.securityContextHolder.removeToken(token);
    }

    /**
     * Return logged user after token
     *
     * @param token
     * @return
     * @throws UnauthorizedException
     * @throws InvalidToken
     */
    @Override
    public UserViewDto getUserAfterToken(String token) throws UnauthorizedException, InvalidToken {
        User user = this.securityContextHolder.getUserAfterToken(token);
        if (user == null) {
            throw new InvalidToken();
        }

        return this.userViewMapper.entityToDto(user);
    }

    /**
     * Create a new user instance
     *
     * @param userViewDto
     * @return UserViewDto
     */
    @Override
    public UserViewDto register(UserViewDto userViewDto) throws ValidationError {
        if (this.userRepository.getUserByUsername(userViewDto.getUsername()) != null) {
            throw new ValidationError("A user with the given username already exist");
        }

        // @to do validation on username and password
        User createdUser = null;

        if (userViewDto.getUsername().length() < 5) {
            throw new ValidationError("Username must have at least 5 characters");
        }

        if (userViewDto.getPassword().length() < 5) {
            throw new ValidationError("Password must have at least 5 characters");
        }

        if (userViewDto.getRole().equals(Role.ROLE_ADMIN)) {
            Admin admin = new Admin();
            admin.setRole(Role.ROLE_ADMIN);
            admin.setPassword(userViewDto.getPassword());
            admin.setUsername(userViewDto.getUsername());

            createdUser = this.adminRepository.save(admin);
        } else if (userViewDto.getRole().equals(Role.ROLE_DOCTOR)) {
            Doctor doctor = new Doctor();
            doctor.setRole(Role.ROLE_DOCTOR);
            doctor.setPassword(userViewDto.getPassword());
            doctor.setUsername(userViewDto.getUsername());

            createdUser = this.doctorRepository.save(doctor);
        } else {
            Patient patient = new Patient();
            patient.setRole(Role.ROLE_PATIENT);
            patient.setPassword(userViewDto.getPassword());
            patient.setUsername(userViewDto.getUsername());

            createdUser = this.patientRepository.save(patient);
        }


        return this.userViewMapper.entityToDto(createdUser);
    }
}
