package com.example.demo.service.veterinaryfacitlity;

import com.example.demo.entities.VeterinaryFacility;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface VeterinaryFacilityService {
    List<VeterinaryFacility> getAll();
}
