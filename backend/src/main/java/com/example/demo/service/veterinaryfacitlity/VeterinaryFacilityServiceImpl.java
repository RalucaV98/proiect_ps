package com.example.demo.service.veterinaryfacitlity;

import com.example.demo.entities.VeterinaryFacility;
import com.example.demo.repository.VeterinaryFacilityRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeterinaryFacilityServiceImpl implements VeterinaryFacilityService {

    private VeterinaryFacilityRepository veterinaryFacilityRepository;

    public VeterinaryFacilityServiceImpl(VeterinaryFacilityRepository veterinaryFacilityRepository) {
        this.veterinaryFacilityRepository = veterinaryFacilityRepository;
    }

    /**
     * Get all veterinary facilities
     * @return List<VeterinaryFacility>
     */
    @Override
    public List<VeterinaryFacility> getAll() {
        return this.veterinaryFacilityRepository.findAll();
    }
}
