package com.example.demo.util;

import java.util.HashMap;

/**
 * Utility class
 * If the key is not inside the hash map will return the default value.
 * @param <K>
 * @param <V>
 */
public class DefaultHashMap<K, V> extends HashMap<K, V> {

    protected V defaultValue;

    public DefaultHashMap(V defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * If hash map contains key will return the corresponding  value
     * If not will return the default value
     * @param k
     */
    @Override
    public V get(Object k) {
        return containsKey(k) ? super.get(k) : defaultValue;
    }
}
