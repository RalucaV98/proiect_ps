insert into user values (1, "pp", "ROLE_PATIENT", "pp");
insert into user values (2, "dd", "ROLE_DOCTOR", "dd");
insert into user values (3, "aa", "ROLE_ADMIN", "aa");

insert into patient values (1);
insert into doctor values (2);
insert into admin values (3);

insert into category values (1, "Mancare Pisici");


insert into veterinary_facility values (1, "Periem orice tip de animal, inclusiv pe tine", "Periere animale", 13.1);
insert into veterinary_facility values (2, "Facem un detarjat complet prin care asiguram ca animalul d-vstra nu va mai avea dinti in gura", "Scoatere dinti", 5.1);
