package com.example.demo;

import com.example.demo.auth.Token;
import com.example.demo.dto.TokenDto;
import com.example.demo.dto.UserViewDto;
import com.example.demo.entities.Role;
import com.example.demo.entities.User;
import com.example.demo.auth.SecurityContextHolder;
import com.example.demo.exceptions.ValidationError;
import com.example.demo.mapper.UserViewMapper;
import com.example.demo.repository.*;
import com.example.demo.service.user.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AuthTest {

    @Mock
    private UserRepository userRepository = Mockito.mock(UserRepository.class, Mockito.RETURNS_DEEP_STUBS);

    @Mock
    private AdminRepository adminRepository = Mockito.mock(AdminRepository.class, Mockito.RETURNS_DEEP_STUBS);

    @Mock
    private DoctorRepository doctorRepository = Mockito.mock(DoctorRepository.class, Mockito.RETURNS_DEEP_STUBS);

    @Mock
    private PatientRepository patientRepository = Mockito.mock(PatientRepository.class, Mockito.RETURNS_DEEP_STUBS);

    private SecurityContextHolder securityContextHolder = new SecurityContextHolder();

    @InjectMocks
    private UserViewMapper userViewMapper;

    private UserServiceImpl userService;

    @Before
    public void before() {
        this.userService = new UserServiceImpl(this.userRepository,
                this.securityContextHolder,
                this.userViewMapper,
                this.adminRepository,
                this.doctorRepository,
                this.patientRepository
        );
    }

    @Test
    public void testSuccessfullyLogin() {
        /**
         * VAlid credentials
         */
        User user = new User();
        user.setPassword("test");
        user.setUsername("test");


        Mockito.when(this.userRepository.getUserByUsernameAndPassword("test", "test")).thenReturn(user);

        try {
            TokenDto tokenDto = this.userService.logIn("test", "test");
            assert this.securityContextHolder.getTokens().size() == 1;
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    public void testUnsuccessfullyLogin() {
        /**
         * Invalid credentials
         */
        User user = new User();
        user.setPassword("test");
        user.setUsername("test");


        Mockito.when(this.userRepository.getUserByUsernameAndPassword("test", "test")).thenReturn(user);

        try {
            this.userService.logIn("test", "password");
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    /**
     * Testing getUserAfterToken method
     */
    @Test
    public void testGetUserAfterToken() {
        User user = new User();
        user.setId(1L);
        user.setPassword("test");
        user.setUsername("test");


        Mockito.when(this.userRepository.getUserByUsernameAndPassword("test", "test")).thenReturn(user);

        try {
            TokenDto token = this.userService.logIn("test","test");
            User u = this.securityContextHolder.getUserAfterToken(token.getToken());

            assert u.getId().equals(user.getId());
        } catch (Exception e) {
            assert false;
        }
    }

    /**
     * Unit test for log out functionality
     * Check if token is succesfully removed from context
     */
    @Test
    public void testRemoveToken() {
        User user = new User();
        user.setId(1L);
        user.setPassword("test");
        user.setUsername("test");


        Mockito.when(this.userRepository.getUserByUsernameAndPassword("test", "test")).thenReturn(user);

        try {
            TokenDto token = this.userService.logIn("test","test");

            assert this.securityContextHolder.getTokens().size() == 1;

            this.userService.logOut(token.getToken());

            assert this.securityContextHolder.getTokens().size() == 0;
        } catch (Exception e) {
            assert false;
        }
    }

    /**
     * Username must be unique
     */
    @Test
    public void testRegisterWithUniqueUsername() {
        User user = new User();
        user.setId(1L);
        user.setPassword("test");
        user.setUsername("test");

        Mockito.when(this.userRepository.getUserByUsername("test")).thenReturn(user);

        try {
            this.userService.register(new UserViewDto(1L, "test", "pass", Role.ROLE_ADMIN));
            assert false;
        } catch (ValidationError e) {
            assert true;
        }
    }
}
