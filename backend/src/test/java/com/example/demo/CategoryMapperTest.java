package com.example.demo;


import com.example.demo.dto.CategoryDto;
import com.example.demo.entities.Category;
import com.example.demo.mapper.CategoryMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;

@RunWith(MockitoJUnitRunner.class)
public class CategoryMapperTest {

    @InjectMocks
    private CategoryMapper categoryMapper;

    @Test
    public void testConvertEntityToDto() {
        Category category = new Category();
        category.setId(1L);
        category.setName("test");
        category.setProductList(new HashSet<>());


        CategoryDto res = this.categoryMapper.entityToDto(category);
        assert res.getId().equals(category.getId()) && res.getName().equals(category.getName());
    }

    @Test
    public void testEntityToDto() {
        CategoryDto categoryDto = new CategoryDto(1L, "tst1");

        Category category = this.categoryMapper.dtoToEntity(categoryDto);

        assert category.getId().equals(categoryDto.getId());
        assert category.getName().equals(categoryDto.getName());
    }
}
