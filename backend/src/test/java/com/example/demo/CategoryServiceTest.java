package com.example.demo;

import com.example.demo.entities.Category;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.service.category.CategoryService;
import com.example.demo.service.category.CategoryServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest  {


    @Mock
    private CategoryRepository categoryRepository = Mockito.mock(CategoryRepository.class, Mockito.RETURNS_DEEP_STUBS);


    @Test
    public void testGetAllCategories() {
        Category category = new Category();
        category.setName("tst1");
        category.setId(1L);
        category.setProductList(new HashSet<>());

        Category category2 = new Category();
        category2.setName("tst1");
        category2.setId(1L);
        category2.setProductList(new HashSet<>());

        List<Category> categoryList = new ArrayList<>();
        categoryList.add(category);
        categoryList.add(category2);

        Mockito.when(this.categoryRepository.findAll()).thenReturn(categoryList);

        CategoryService categoryService = new CategoryServiceImpl(this.categoryRepository);

        assert categoryService.getAllCategories().size() == 2;
    }
}
