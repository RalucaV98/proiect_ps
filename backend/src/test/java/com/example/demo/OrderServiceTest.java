package com.example.demo;

import com.example.demo.entities.Order;
import com.example.demo.entities.Product;
import com.example.demo.entities.ShoppingCart;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.mapper.ShoppingCartMapper;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.ShoppingCartRepository;
import com.example.demo.service.order.OrderService;
import com.example.demo.service.order.OrderServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
class OrderServiceTest {

	@Mock
	private OrderRepository orderRepository = Mockito.mock(OrderRepository.class, Mockito.RETURNS_DEEP_STUBS);

	@Mock
	private ShoppingCartRepository shoppingCartRepository = Mockito.mock(ShoppingCartRepository.class, Mockito.RETURNS_DEEP_STUBS);


	@Autowired
	private ShoppingCartMapper shoppingCartMapper;

	@Autowired
	private OrderMapper orderMapper;

	@Test
	public void testGetMostSoldProducts() {
		Order order = new Order();
		order.setTotalPrice(23D);

		List<Product> productList = new ArrayList<>();
		Product p1 = new Product("p1", 23.0, 2);
		p1.setId(1L);
		Product p2 = new Product("p2", 23.0, 2);
		p2.setId(2L);

		productList.add(p1);
		productList.add(p1);
		productList.add(p2);

		ShoppingCart shoppingCart = new ShoppingCart();
		shoppingCart.setId(1L);
		shoppingCart.setProducts(productList);

		order.setShoppingCart(shoppingCart);
		List<Order> orderList = new ArrayList<>();
		orderList.add(order);

		Mockito.when(this.orderRepository.findAll()).thenReturn(orderList);

		OrderService orderService = new OrderServiceImpl(this.orderRepository, this.shoppingCartRepository, this.orderMapper);

		Map<Product, Long> productsCnt = orderService.getMostSoldProducts(1);

		assert productsCnt.size() == 1;
		assert productsCnt.get(p1) == 2;
		assert !productsCnt.containsKey(p2);
	}
}
