package com.example.demo;

import com.example.demo.dto.ProductDto;
import com.example.demo.entities.Category;
import com.example.demo.entities.Product;
import com.example.demo.mapper.CategoryMapper;
import com.example.demo.mapper.ProductMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

@RunWith(MockitoJUnitRunner.class)
public class ProductMapperTest {

    @InjectMocks
    private ProductMapper productMapper;


    @Test
    public void testConvertEntityToDto(){
        CategoryMapper categoryMapper = new CategoryMapper();

        this.productMapper = new ProductMapper(categoryMapper);

        Category category = new Category();
        category.setId(1L);
        category.setName("tst");

        Product product = new Product();
        product.setId(1L);
        product.setQuantity(2);
        product.setPrice(23D);
        product.setName("txt");
        product.setCarts(new ArrayList<>());
        product.setCategory(category);

        ProductDto productDto = this.productMapper.entityToDto(product);

        assert productDto.getId().equals(product.getId());
        assert productDto.getName().equals(product.getName());
        assert productDto.getPrice().equals(product.getPrice());
        assert productDto.getCategoryDto().getId().equals(product.getCategory().getId());
        assert productDto.getQuantity().equals(product.getQuantity());
    }
}
