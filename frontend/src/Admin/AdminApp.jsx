import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {constants} from "../constants";
import {Dashboard} from "./Dashboard";
import {ProductModel} from "./Model/ProductModel";
import {CategoryModel} from "./Model/CategoryModel";
import {PrivateRoute} from "../Router/PrivateRouter";
import {permissions} from "../permissions/permissions";
import {NotFoundPage} from "../NotFound/NotFoundPage";

class AdminApp extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <Router>
                <Switch>
                    <PrivateRoute exact path={constants.routes.app.adminDashboard} component={Dashboard}
                                  hasPermission={permissions.isLogged()}
                                  redirect={constants.routes.app.login}/>
                    <PrivateRoute exact path={constants.routes.app.productBaseCrud} component={ProductModel}
                                  hasPermission={permissions.isLogged()} redirect={constants.routes.app.login}/>
                    <PrivateRoute exact path={constants.routes.app.categoryBaseCrud} component={CategoryModel}
                                  hasPermission={permissions.isLogged()} redirect={constants.routes.app.login}/>
                    <Route component={NotFoundPage}/>
                </Switch>
            </Router>
        )
    }
}

export {AdminApp};