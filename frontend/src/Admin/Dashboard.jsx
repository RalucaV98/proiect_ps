import React from "react";
import Navbar from "./Navbar/NavBar";
import {Link} from "react-router-dom";
import {constants} from "../constants";
import {MostSoldProductsReportForm} from "./Form/MostSoldProductsForm";

class Dashboard extends React.Component {
    constructor(props) {
        super(props);


    }

    render() {
        return (
            <div>
                <Navbar/>

                <div className={"page-content"}>
                    <h1>Admin dashboard</h1>

                    <p>
                        <h2>Models</h2>
                        <ul>
                            <li>
                                <Link to={constants.routes.app.productBaseCrud}>Products </Link>
                            </li>
                            <li>
                                <Link to={constants.routes.app.categoryBaseCrud}>Category </Link>
                            </li>
                        </ul>
                    </p>

                    <p>
                        <h2>Reports</h2>
                        <ul>
                            <li>
                                Generate most sold products

                                <MostSoldProductsReportForm/>
                            </li>
                        </ul>
                    </p>
                </div>
            </div>
        )
    }
}

export {Dashboard};