import React from "react";
import Select from 'react-select';
import {reportService} from "../../service/report.services";
import {constants} from "../../constants";


class MostSoldProductsReportForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            typeSelected: null,
            options: [{
                label: 'txt',
                value: 'TXT'
            }, {
                label: 'xls',
                value: 'XLS'
            }]
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit = e => {
        e.preventDefault();

        window.open(constants.routes.api.generateReportUrl + this.state.typeSelected.value + '/', "_blank");
    };

    handleChange = typeSelected => {
        this.setState({typeSelected});
    };

    render() {

        return (
            <form className={"form"} onSubmit={this.handleSubmit}>
                <div className={"form-group"}>
                    <label htmlFor={"type"}>Choose type</label>
                    <Select options={this.state.options} name={"type"} value={this.typeSelected}
                            onChange={this.handleChange}/>
                </div>

                <button className={"btn btn-primary"} type={"submit"} disabled={this.state.typeSelected === null}>Generate report</button>
            </form>
        )
    }
}

export {MostSoldProductsReportForm};