import React from "react";
import Navbar from "../Navbar/NavBar";
import {Link} from "react-router-dom";
import {constants} from "../../constants";
import {productService} from "../../service";
import {TableComponent} from "../Table/TableComponent";
import {DeleteBox} from "./DeleteBox/DeleteBox";
import {categoryService} from "../../service/category.service";
import {CategoryForm} from "./Forms";
import Select from "react-select";

class CategoryModel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            categorySelected: null,
            fields: ['id', 'name']
        };

        this.deleteCategory = this.deleteCategory.bind(this);
        this.callback = this.callback.bind(this);
    }

    componentDidMount() {
        categoryService.getAll().then(
            categories => {
                this.setState({categories});
            }
        )
    }

    deleteCategory(category) {

        categoryService.deleteById(category.value).then(
            res => {
                let filtered = this.state.categories.filter(p => {
                    return p.id !== category.value
                });

                this.setState({categories: filtered});
            }, error => {
                console.log(error);
            }
        );
    }

    callback(obj) {
        if (obj.edit) {
            window.location.reload();
        } else {
            let categories = this.state.categories.concat(obj.category);
            this.setState({categories: categories});
        }
    }

    handleChangeCategory = categorySelected => {
        this.setState({categorySelected});
    };


    render() {
        let categoryDto = null;

        if (this.state.categorySelected !== null) {

            for (let i = 0; i < this.state.categories.length; i++) {
                if (this.state.categories[i].id === this.state.categorySelected.value) {
                    categoryDto = this.state.categories[i];
                    break;
                }
            }

        }

        const options = this.state.categories.map((category) => {
                return {
                    value: category.id,
                    label: category.name
                };
            }
        );

        return (
            <div>
                <Navbar/>

                <div className={"page-content"}>
                    <h1>Categories</h1>

                    <TableComponent objects={this.state.categories} fields={this.state.fields}/>

                    <div className={"container"}>
                        <div className={"row"}>
                            <div className={"col-md-6"}>
                                <h2>Delete category</h2>
                                <DeleteBox options={options} callback={this.deleteCategory}/>
                            </div>
                            <div className={"col-md-6"}>
                                <h2>Create category</h2>
                                <CategoryForm callback={this.callback}/>
                            </div>
                        </div>

                        <div className={"row"}>
                            <h2>Edit category</h2>

                            <div className={"container"}>
                                <div className={"row"}>
                                    <div className={"col-md-6"}>
                                        <div className={"form-group"}>
                                            <label htmlFor={"category-selected-for-edit"} className={""}>Choose
                                                category</label>
                                            <Select options={options} name={"category-selected-for-edit"}
                                                    onChange={this.handleChangeCategory}/>
                                        </div>
                                    </div>
                                    <div className={"col-md-6"}>
                                        {this.state.categorySelected !== null &&
                                        <CategoryForm callback={this.callback} category={categoryDto}/>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export {CategoryModel};