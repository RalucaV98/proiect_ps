import React from "react";
import Select from 'react-select'

class DeleteBox extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            options: props.options,
            objectSelected: null,
            callback: props.callback
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({options: nextProps.options})
    }


    handleChange = objectSelected => {
        this.setState({objectSelected});
    };

    handleSubmit(e) {
        e.preventDefault();

        if (this.state.objectSelected === null) {
            alert("You have to choose an object");
            return;
        }

        this.props.callback(this.state.objectSelected);
    }

    render() {
        return (
            <form name={"form"} onSubmit={this.handleSubmit}>
                <label for={"select_obj"}>Choose model</label>
                <Select options={this.state.options} name={"select_obj"} value={this.state.objectSelected} onChange={this.handleChange}/>
                <button type={"submit"} className={"btn btn-primary"}>Delete</button>
            </form>
        )
    }
}

export {DeleteBox};