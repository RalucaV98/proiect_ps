import React from "react";
import {categoryService} from "../../../service/category.service";
import Select from "react-select";
import {productService} from "../../../service";

class CategoryForm extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            id: (props.category) ? props.category.id: null,
            name: (props.category) ? props.category.name: "",
            edit: (props.category)
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.category) {
            this.setState({id: nextProps.category.id, name: nextProps.category.name, edit: true});
        }
    }

    handleSubmit(e) {
        e.preventDefault();

        let obj = {
            name: this.state.name,
            id: (this.state.edit) ? this.state.id: null,
        };

        categoryService.save(obj).then(
            category => {
                this.props.callback({edit: this.state.edit, category: category});
            }, err => {
                console.log(err);
            }
        );
    }

    validate() {
        return !(this.state.name);
    }


    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }


    render() {
        return (
            <form className={"form"} onSubmit={this.handleSubmit}>
                <div className={"form-group"}>
                    <label htmlFor={"name"}>Name</label>
                    <input className={"form-control"} type={"text"} name={"name"} value={this.state.name} onChange={this.handleChange}/>
                </div>

                <button type={"submit"} className={"btn btn-primary"} disabled={this.validate()}>
                    {this.state.edit ? "Edit": "Create"}
                </button>
            </form>
        )
    }
}

export {CategoryForm};