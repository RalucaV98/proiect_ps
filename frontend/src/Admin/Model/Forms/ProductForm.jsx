import React from "react";
import {categoryService} from "../../../service/category.service";
import Select from "react-select";
import {productService} from "../../../service";
import ImageUploader from 'react-images-upload';

class ProductForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            category: null,
            name: (props.product) ? props.product.name : "",
            price: (props.product) ? props.product.price : 0.0,
            quantity: (props.product) ? props.product.quantity : 0.0,
            categories: [],
            categorySelected: (props.product) ? {
                value: props.product.categoryDto.id,
                label: props.product.categoryDto.name
            } : null,
            id: (props.product) ? props.product.id : null,
            base64Photo: (props.product) ? props.product.base64Photo: "",
            edit: (props.product),
            pictures: [],
            isUploaded: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onDrop = this.onDrop.bind(this);
    }

    componentDidMount() {
        categoryService.getAll().then(
            categories => {
                this.setState({categories});
            }
        )
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.product) {
            this.setState({
                name: nextProps.product.name,
                price: nextProps.product.price,
                quantity: nextProps.product.quantity,
                id: nextProps.product.id,
                base64Photo: nextProps.product.base64Photo,
                edit: true,
                categorySelected: {
                    value: nextProps.product.categoryDto.id,
                    label: nextProps.product.categoryDto.name
                }
            })
        }

    }

    async handleSubmit(e) {
        e.preventDefault();

        if (this.state.categorySelected === null) {
            alert("You have to choose a category");
            return;
        }

        if (!this.state.name || this.state.price < 0) {
            alert("Invalid name or price");
            return;
        }

        let categoryDto = null;

        for (let i = 0; i < this.state.categories.length; i++) {
            if (this.state.categories[i].id === this.state.categorySelected.value) {
                categoryDto = this.state.categories[i];
                break;
            }
        }


        let obj = {
            name: this.state.name,
            price: this.state.price,
            quantity: this.state.quantity,
            id: (this.state.edit) ? this.state.id : null,
            categoryDto: categoryDto,
            base64Photo: this.state.base64Photo
        };

        if (this.state.edit && this.state.pictures.length > 0) {
            productService.updateProductImage(this.state.id,
                this.state.pictures[this.state.pictures.length - 1]).then(
                product => {
                    obj.base64Photo = product.base64Photo;

                    productService.save(obj).then(
                        p => {
                            this.props.callback({edit: this.state.edit, product: p});
                        }, error => {
                            console.log(error);
                        }
                    )
                }
            )
        } else {
            productService.save(obj).then(
                p => {
                    this.props.callback({edit: this.state.edit, product: p});
                }, error => {
                    console.log(error);
                }
            )
        }


    }

    onDrop(picture) {
        if (picture.length === 0) {
            return;
        }
        this.setState({
            pictures: this.state.pictures.concat(picture),
            isUploaded: true
        });
    }

    validate() {
        return !(this.state.name && this.state.price > 0 && this.state.quantity > 0 && this.state.categorySelected !== null);
    }


    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    handleCategoryChange = categorySelected => {
        this.setState({categorySelected})
    };

    render() {
        const options = this.state.categories.map((c, k) => {
            return {
                label: c.name,
                value: c.id
            }
        });

        let src = "";

        if (this.state.isUploaded) {
            try {
                src = URL.createObjectURL(this.state.pictures[this.state.pictures.length - 1]);
            } catch (e) {
                src = "data:image/jpg;base64," + this.state.base64Photo;
            }
        } else {
            src = "data:image/jpg;base64," + this.state.base64Photo;
        }

        return (
            <form className={"form"} onSubmit={this.handleSubmit}>
                <div className={"form-group"}>
                    <label htmlFor={"name"}>Name</label>
                    <input className={"form-control"} type={"text"} name={"name"} value={this.state.name}
                           onChange={this.handleChange}/>
                </div>
                <div className={"form-group"}>
                    <label htmlFor={"price"}>Price</label>
                    <input className={"form-control"} type="number" name={"price"} value={this.state.price}
                           onChange={this.handleChange}/>
                </div>
                <div className={"form-group"}>
                    <label htmlFor={"quantity"}>Quantity</label>
                    <input className={"form-control"} type="number" name={"quantity"} value={this.state.quantity}
                           onChange={this.handleChange}/>
                </div>
                <div className={"form-group"}>
                    <label htmlFor={"category"}>Choose category</label>
                    <Select options={options} name={"category"} value={this.state.categorySelected}
                            onChange={this.handleCategoryChange}/>
                </div>

                {this.state.edit &&
                <div className="form-group">
                    <img className={"profile-pic"} style={{height: "200px", width: "200px"}} src={src}/>

                    <ImageUploader
                        withIcon={false}
                        buttonText='Choose image'
                        onChange={this.onDrop}
                        imgExtension={['.jpg', '.png',]}
                        maxFileSize={65535}
                    />
                </div>
                }

                <button type={"submit"} className={"btn btn-primary"} disabled={this.validate()}>
                    {this.state.edit ? "Edit" : "Create"}
                </button>
            </form>
        )
    }
}

export {ProductForm};