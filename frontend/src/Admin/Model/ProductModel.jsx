import React from "react";
import Navbar from "../Navbar/NavBar";
import {productService} from "../../service";
import {TableComponent} from "../Table/TableComponent";
import {DeleteBox} from "./DeleteBox/DeleteBox";
import {ProductForm} from "./Forms";
import Select from "react-select";


class ProductModel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            productSelected: null,
            service: props.service,
            fields: ['id', 'name', 'price']
        };

        this.deleteProduct = this.deleteProduct.bind(this);
        this.callback = this.callback.bind(this);
    }

    componentDidMount() {
        productService.getAll().then(
            products => {
                this.setState({products});
            }
        )
    }

    deleteProduct(product) {

        productService.deleteById(product.value).then(
            res => {
                let filtered = this.state.products.filter( p => {
                    return p.id !== product.value
                });

                this.setState({products: filtered});
            }, error => {
                console.log(error);
            }
        );
    }

    callback(obj) {
        if (obj.edit) {
            window.location.reload();
        } else {
            let products = this.state.products.concat(obj.product);
            this.setState({products: products});
        }
    }

    handleProductChange = productSelected => {
        this.setState({productSelected});
    };

    render() {
        let productDto = null;

        if (this.state.productSelected !== null) {

            for (let i = 0; i < this.state.products.length; i++) {
                if (this.state.products[i].id === this.state.productSelected.value) {
                    productDto = this.state.products[i];
                    break;
                }
            }

        }

        const options = this.state.products.map((product) => {
                return {
                    value: product.id,
                    label: product.name
                };
            }
        );

        return (
            <div>
                <Navbar/>

                <div className={"page-content"}>
                    <h1>Products</h1>

                    <TableComponent objects={this.state.products} fields={this.state.fields}/>

                    <div className={"container"}>
                        <div className={"row"}>
                            <div className={"col-md-6"}>
                                <h2>Delete product</h2>
                                <DeleteBox options={options} callback={this.deleteProduct}/>
                            </div>
                            <div className={"col-md-6"}>
                                <h2>Create product</h2>
                                <ProductForm callback={this.callback}/>
                            </div>
                        </div>
                        <div className={"row"}>
                            <h2>Edit product</h2>

                            <div className={"container"}>
                                <div className={"row"}>
                                    <div className={"col-md-6"}>
                                        <div className={"form-group"}>
                                            <label htmlFor={"category-product-for-edit"} className={""}>Choose
                                                product</label>
                                            <Select options={options} name={"category-selected-for-edit"}
                                                    onChange={this.handleProductChange}/>
                                        </div>
                                    </div>

                                    <div className={"col-md-6"}>
                                        {this.state.productSelected &&
                                            <ProductForm callback={this.callback} product={productDto}/>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export {ProductModel};