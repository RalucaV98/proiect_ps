import React from "react";

class TableComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            objects: props.objects,
            fields: props.fields
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({objects: nextProps.objects, fields: nextProps.fields})
    }


    render() {
        return (
            <table className="table">
                <thead>
                <tr>
                    {this.state.fields.map((f, k) => {
                        return (
                            <th key={k} scope={"col"}>{f}</th>
                        )
                    })}
                </tr>
                </thead>
                <tbody>
                {this.state.objects.map((obj, key) => {
                    return (
                        <tr key={key}>
                            {this.state.fields.map((f, k) => {
                                    return (<td key={k}>{obj[f]}</td>)
                                }
                            )}

                        </tr>
                    )
                })}
                </tbody>
            </table>
        )
    }
}

export {TableComponent};