import * as React from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {LoginComponent} from "../Login";
import {RegisterComponent} from "../Register";
import './bootstrap.min.css';
import './mdb.lite.min.css';
import './style.min.css';
import './index.css';
import {constants} from "../service";
import PatientApp from "../Patient/PatientApp";
import {DoctorApp} from "../Doctor";
import {AdminApp} from "../Admin";
import {PrivateRoute} from "../Router/PrivateRouter";
import {permissions} from "../permissions/permissions";
import {NotFoundPage} from "../NotFound/NotFoundPage";


class App extends React.Component {
    render() {
        return (
            <div className={"container"}>
                <Router>
                    <Switch>
                        <Route path={constants.routes.app.login} component={LoginComponent}/>
                        <Route path={constants.routes.app.register} component={RegisterComponent}/>
                        <PrivateRoute path={constants.routes.app.patientBase} component={PatientApp}
                                      hasPermission={permissions.isPatient()}
                                      redirect={constants.routes.app.notFound}/>
                        <PrivateRoute path={constants.routes.app.doctorBase} component={DoctorApp}
                                      hasPermission={permissions.isDoctor()}
                                      redirect={constants.routes.app.notFound}
                        />
                        <PrivateRoute path={constants.routes.app.adminBase} component={AdminApp}
                                      hasPermission={permissions.isAdmin()}
                                      redirect={constants.routes.app.notFound}
                        />
                        <Route component={NotFoundPage}/>
                    </Switch>
                </Router>
            </div>
        )
    }
}

export default App;

