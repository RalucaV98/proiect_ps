import * as React from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {LoginComponent} from "../Login";
import {RegisterComponent} from "../Register";
import {HomeComponent} from "../Home";
import './bootstrap.min.css';
import './mdb.lite.min.css';
import './style.min.css';
import './index.css';
import {CartComponent} from "../Cart";
import {cartService, constants} from "../service";
import {fetchCart} from "../js/actions";
import {connect} from "react-redux";
import {PrivateRoute} from "../components";
import {VetClinicPage} from "../VetClinic/VetClinicPage";


function mapDispatchToProps(dispatch) {
    return {
        fetchCart: cart => dispatch(fetchCart(cart))
    }
}

class PatientAppUtil extends React.Component {

    componentDidMount() {
        if (localStorage.getItem(constants.savedToken)) {

            cartService.getLoggedUserCart().then(
                cart => {
                    this.props.fetchCart(cart);
                }
            )
        }
    }

    render() {
        return (
            <Router>
                <PrivateRoute exact path={constants.routes.app.shop} component={HomeComponent} after={constants.savedToken}
                              redirect={constants.routes.app.login}/>
                <PrivateRoute exact path={constants.routes.app.cart} component={CartComponent} after={constants.savedToken} redirect={"/login"}/>
                <PrivateRoute exact path={constants.routes.app.vetClinic} component={VetClinicPage} after={constants.savedToken} redirect={"/login"}/>
            </Router>
        )
    }
}

const PatientApp = connect(null, mapDispatchToProps)(PatientAppUtil);
export default PatientApp;