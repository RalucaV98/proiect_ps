import React from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {constants} from "../constants";
import {ReservationsPage} from "./Reservation";
import {PrivateRoute} from "../Router/PrivateRouter";
import {permissions} from "../permissions/permissions";
import {NotFoundPage} from "../NotFound/NotFoundPage";

class DoctorApp extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <Switch>
                    <PrivateRoute exact path={constants.routes.app.reservationsPage} component={ReservationsPage}
                                  hasPermission={permissions.isLogged()}
                                  redirect={constants.routes.app.login}/>
                    <Route component={NotFoundPage}/>
                </Switch>
            </Router>
        )
    }
}

export {DoctorApp};