import {constants} from "../../constants";
import {CartMinuature} from "../../Patient/components";
import {userService} from "../../service";
import React from "react";

const Navbar = ()=>{
    return(

        <nav className="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div className="container">

                <a className="navbar-brand waves-effect" href={constants.routes.app.reservationsPage}>
                    <strong className="blue-text">Reservations</strong>
                </a>

                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">

                    <ul className="navbar-nav mr-auto">
                    </ul>

                    <ul className="navbar-nav nav-flex-icons">
                        <li className="nav-item">
                            <a className="nav-link waves-effect" onClick={() => userService.logout()}>
                                <i className="fas fa-sign-out-alt"/>
                                <span className="clearfix d-none d-sm-inline-block"> Log out </span>
                            </a>
                        </li>
                    </ul>

                </div>

            </div>
        </nav>
    )
};

export default Navbar;