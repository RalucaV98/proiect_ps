import React from "react";
import Navbar from "../Navbar/NavBar";
import {appointmentService} from "../../service/appointment.service";


class ReservationsPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            appointments: [],
            error: ""
        }
    }

    componentDidMount() {
        appointmentService.getActiveAppointments().then(
            appointments => {
                this.setState({appointments});
            }, error => {
                this.setState({error: error.message})
            }
        )
    }

    render() {
        console.log(this.state.appointments);
        return (
            <div>
                <Navbar/>

                <div className={"page-content"}>
                    <h1>List of active reservations</h1>

                    {this.state.error &&
                    <div className={"alert alert-danger"}>
                        {this.state.error}
                    </div>
                    }

                    {this.state.appointments.map( (a, key) => {
                        return (
                            <div key={key} className={"card appointment-cart"}>
                                <p>
                                    <strong>Date:</strong> {a.date}
                                </p>

                                <p>
                                    <strong>Patient:</strong> {a.patientDto.username}
                                </p>

                                <p>
                                    <strong>Service:</strong> {a.veterinaryFacility.name}
                                </p>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export {ReservationsPage}