import React from "react";
import {Link} from "react-router-dom";
import {permissions} from "../permissions/permissions";
import {constants} from "../constants";

class NotFoundPage extends React.Component {

    render() {
        let linkTo = "";
        if (permissions.isAdmin()) {
            linkTo = constants.routes.app.adminDashboard
        } else if (permissions.isPatient()) {
            linkTo = constants.routes.app.shop;
        } else if (permissions.isDoctor()) {
            linkTo = constants.routes.app.reservationsPage;
        } else {
            linkTo = constants.routes.app.login;
        }

        return (
            <div className={"container align-center"}>
                <p>404 not found</p>
                <Link to={linkTo} className={"btn btn-primary"}>Take me home</Link>
            </div>
        )
    }
}

export {NotFoundPage};