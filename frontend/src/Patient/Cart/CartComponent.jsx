import React from 'react';
import Navbar from "../components/NavBar";
import {connect} from "react-redux";
import {productService, userService} from "../../service";
import {clearCart, fetchCart} from "../../redux/actions";
import {orderService} from "../../service/order.service";

const mapStateToProps = state => {
    return {products: state.products, cart_id: state.cart_id}
};

function mapDispatchToProps(dispatch) {
    return {
        clearCart: () => dispatch(clearCart()),
        fetchInitial: (cart) => dispatch(fetchCart(cart))
    }
}

class ProductsFormMinuature extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            product: props.product,
            cart_id: props.cart_id
        };

        this.submit = this.submit.bind(this);
        this.change = this.change.bind(this);
    }

    submit(e) {
        e.preventDefault();

        productService.removeFromCart(this.state.cart_id, this.state.product.id).then(
            cart => {
                this.props.fetchInitial(cart);
            }, error => {
                console.log(error);
            }
        )
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({product: nextProps.product, cart_id: nextProps.cart_id})
    }

    change(e) {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    render() {
        const {product} = this.state;

        return (
            <li className="list-group-item d-flex justify-content-between lh-condensed">
                <h6 className="my-0">{product.name}</h6>
                <button className="btn btn-primary btn-md my-0 p" type="submit" onClick={this.submit}> Remove
                    <i className="fas fa-shopping-cart ml-1"></i>
                </button>
                {/*</form>*/}
            </li>
        )
    }
}

class CartComponentPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: props.products,
            cart_id: props.cart_id
        };

        this.placeOrder = this.placeOrder.bind(this);
    }


    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({products: nextProps.products, cart_id: nextProps.cart_id});
    }

    placeOrder(e) {
        e.preventDefault();

        orderService.placeOrder(this.state.cart_id).then(
            order => {
                this.props.clearCart()
            }, error => {
                console.log(error);
            }
        )
    }

    render() {
        const productsListing = this.state.products.map((product, key) => {
            return (
                <li className="list-group-item d-flex justify-content-between lh-condensed" key={key}>
                    <div>
                        <h6 className="my-0">1 x {product.name}</h6>
                    </div>
                    <span className="text-muted">LEI {product.price}</span>
                </li>
            )
        });
        let totalPrice = 0;
        for (let i = 0; i < this.state.products.length; i++) {
            totalPrice += this.state.products[i].price;
        }

        return (
            <div>
                <Navbar/>
                <main className={"mt-5 pt-4"}>
                    <div className="container wow fadeIn">

                        <h2 className="my-5 h2 text-center">Checkout</h2>

                        <div className="row">

                            <div className="col-md-5 mb-3">
                                <h4 className="d-flex justify-content-between align-items-center mb-3">
                                    <span className="text-muted">Products</span>
                                    <span className="badge blue badge-pill ">{this.state.products.length}</span>
                                </h4>

                                <ul className="list-group mb-3 z-depth-1">
                                    {this.state.products.map((p, key) => {
                                        return (
                                            <ProductsFormMinuature key={key} product={p} cart_id={this.state.cart_id}
                                                                   fetchInitial={this.props.fetchInitial}/>)
                                    })}
                                </ul>
                            </div>
                            <div className="col-md-7 mb-9">

                                <h4 className="d-flex justify-content-between align-items-center mb-3">
                                    <span className="text-muted">Your cart</span>
                                    <span className="badge blue badge-pill ">{this.state.products.length}</span>
                                </h4>

                                <ul className="list-group mb-3 z-depth-1">
                                    {productsListing}
                                    <li className="list-group-item d-flex justify-content-between">
                                        <span>Total (LEI)</span>
                                        <strong>LEI {totalPrice}</strong>
                                    </li>
                                </ul>

                                <form className="card p-2">
                                    <button className="btn btn-primary btn-md waves-effect m-0"
                                            type="button" onClick={this.placeOrder} disabled={this.state.products.length === 0}>Place order
                                    </button>
                                </form>

                            </div>

                        </div>

                    </div>
                </main>

            </div>
        )
    }
}

const CartComponent = connect(mapStateToProps, mapDispatchToProps)(CartComponentPage);
export {CartComponent}