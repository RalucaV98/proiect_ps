import React from 'react';
import Navbar from "../components/NavBar";
import {ProductsListing} from "../components/ProductsListing";
import {productService} from "../../service";
import queryString from 'query-string';

class HomeComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            error: "",
        };
    }

    componentDidMount() {
        productService.getAll().then(
            products => {
                this.setState({products})
            }
        )
    }

    render() {
        const {products, error} = this.state;
        return (
            <div>
                <Navbar/>

                <div className={"page-content"}>
                    {error &&
                    <div className={"alert alert-danger"}>{error}</div>
                    }
                    {!error &&
                    <ProductsListing products={products}
                    />
                    }
                </div>

            </div>
        )
    }
}


export {HomeComponent};