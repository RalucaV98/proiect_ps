import * as React from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
// import {LoginComponent} from "../Login";
// import {RegisterComponent} from "../Register";
import {HomeComponent} from "./Home";
// import '../App/bootstrap.min.css';
// import './mdb.lite.min.css';
// import './style.min.css';
// import './index.css';
import {CartComponent} from "./Cart";
import {cartService, constants} from "../service";
import {fetchCart} from "../redux/actions";
import {connect} from "react-redux";
import {VetClinicPage} from "./VetClinic/VetClinicPage";
import {PrivateRoute} from "../Router/PrivateRouter";
import {permissions} from "../permissions/permissions";
import {NotFoundPage} from "../NotFound/NotFoundPage";


function mapDispatchToProps(dispatch) {
    return {
        fetchCart: cart => dispatch(fetchCart(cart))
    }
}

class PatientAppUtil extends React.Component {

    componentDidMount() {
        if (localStorage.getItem(constants.savedToken)) {

            cartService.getLoggedUserCart().then(
                cart => {
                    this.props.fetchCart(cart);
                }
            )
        }
    }

    render() {
        return (
            <Router>
                <Switch>
                    <PrivateRoute exact path={constants.routes.app.shop} component={HomeComponent}
                                  hasPermission={permissions.isLogged()}
                                  redirect={constants.routes.app.login}/>
                    <PrivateRoute exact path={constants.routes.app.cart} component={CartComponent}
                                  hasPermission={permissions.isLogged()} redirect={constants.routes.app.login}/>
                    <PrivateRoute exact path={constants.routes.app.vetClinic} component={VetClinicPage}
                                  hasPermission={permissions.isLogged()} redirect={constants.routes.app.login}/>
                    <Route component={NotFoundPage}/>
                </Switch>
            </Router>
        )
    }
}

const PatientApp = connect(null, mapDispatchToProps)(PatientAppUtil);
export default PatientApp;