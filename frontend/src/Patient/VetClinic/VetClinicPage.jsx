import React from 'react';
import Navbar from "../components/NavBar";
import {vetClinicService} from "../../service/vet.clinic.service";
import Select from 'react-select'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {constants} from "../../constants";

class VetClinicPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            services: [],
            selectedService: null,
            selectedDate: new Date(),
            submitted: false,
            error: "",
            successMessage: ""
        };

        this.onchange = this.onchange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    componentDidMount() {
        vetClinicService.getAllVeterinaryServices().then(
            services => {
                this.setState({services});
            }
        )
    }

    onchange(e) {
        const {name, value} = e.target;

        this.setState({[name]: value});
    }

    handleSubmit = e => {
        e.preventDefault();

        if (this.state.selectedService === null) {
            this.setState({submitted: true});
            return;
        }

        let facility = {};

        for (let i = 0; i < this.state.services.length; i++) {
            if (this.state.services[i].id === this.state.selectedService.value) {
                facility = this.state.services[i];
                break;
            }
        }

        vetClinicService.createAppointment({
            patientDto: JSON.parse(localStorage.getItem(constants.loggedUser)),
            date: this.state.selectedDate,
            veterinaryFacility: facility
        }).then(
            data => {
                console.log(data);

                this.setState({successMessage: "Your appointment was created", error: ""})
            }, error => {
                this.setState({error: error.message, successMessage: ""})
            }
        );
    };

    handleSelectedService = selectedService => {
        this.setState({selectedService});
    };


    handleDateChange = selectedDate => {
        this.setState({selectedDate});
    };

    render() {
        const options = this.state.services.map((s) => {
            return {
                'value': s.id,
                'label': s.name
            }
        });

        return (
            <div>
                <Navbar/>

                <div className={"page-content"}>
                    <h1>Vet clinic</h1>

                    <div className={"services-list"}>

                        <div className={"row"}>

                            <div className={"col-md-6 col-lg-6"}>
                                <h3>Our services</h3>
                                <ul>
                                    {this.state.services.map(
                                        (s) => {
                                            return (
                                                <li>
                                                    <strong>{s.name}</strong>
                                                    <p>
                                                        {s.description}
                                                    </p>
                                                    <p>
                                                        Price: {s.price} LEI
                                                    </p>
                                                </li>)
                                        }
                                    )}
                                </ul>
                            </div>

                            <div className="col-md-6 col-lg-6">
                                <h3>Create an appointment</h3>

                                <form className={"form"} onSubmit={this.handleSubmit}>
                                    <div
                                        className={'form-group'}>
                                        <label htmlFor='services'> Select service </label>
                                        <Select name={'services'} options={options} value={this.state.selectedService}
                                                onChange={this.handleSelectedService}/>
                                        {this.state.submitted && this.state.selectedService === null &&
                                        <div className={"alert alert-danger"}>This field is required</div>
                                        }
                                    </div>

                                    <div
                                        className={'form-group'}>
                                        <label htmlFor='selectedDate'> Select a date </label>
                                        <DatePicker
                                            name={"selectedDate"}
                                            selected={this.state.selectedDate}
                                            onChange={this.handleDateChange}
                                        />
                                    </div>
                                    <button type={"submit"} className={"btn btn-primary"}>
                                        Submit
                                    </button>
                                    {this.state.error &&
                                    <div className={"alert alert-danger"}>
                                        {this.state.error}
                                    </div>
                                    }
                                    {this.state.successMessage &&
                                        <div className={"alert alert-info"}>
                                            {this.state.successMessage}
                                        </div>
                                    }
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )

    }
}

export {VetClinicPage}