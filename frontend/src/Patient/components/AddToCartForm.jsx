import React from "react";
import {productService} from "../../service";
import {addProductToCart, clearCart, fetchCart, removeProductFromCart} from "../../redux/actions";
import {connect} from "react-redux";
import {ToastH} from "./Toast";

function mapDispatchToProps(dispatch) {
    return {
        fetchInitial: (cart) => dispatch(fetchCart(cart)),
        addProduct: (product) => dispatch(addProductToCart(product)),
        removeProduct: (product) => dispatch(removeProductFromCart(product))
    }
}

function mapStateToProps(state) {
    return {productsInCart: state.products, cart_id: state.cart_id}
}

class AddToCartFormR extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            product: props.product,
            show: false,
            message: "",
            error: "",
            productsInCart: [],
            cart_id: props.cart_id
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            product: nextProps.product,
            productsInCart: nextProps.productsInCart,
            cart_id: nextProps.cart_id
        })
    }

    handleSubmit(e) {
        e.preventDefault();


        let isInCart = false;

        this.setState({error: ""});

        for (let i = 0; i < this.state.productsInCart.length; i++) {
            if (this.state.productsInCart[i].id === this.state.product.id) {
                isInCart = true;
            }
        }

        if (isInCart) {
            productService.removeFromCart(this.state.cart_id, this.state.product.id).then(
                data => {
                    this.props.fetchInitial(data);
                }, error => {
                    this.setState({error: "Please try again later"});
                }
            )
        } else {
            productService.addToCart(this.state.cart_id, this.state.product.id).then(
                data => {
                    this.props.fetchInitial(data);
                }, error => {
                    this.setState({error: "Please try again later"});
                    console.log(error);
                }
            )
        }
    }

    handleChange(e) {
        const {name, value} = e.target;

        this.setState({[name]: value});
    }

    render() {
        let isInCart = false;

        for (let i = 0; i < this.state.productsInCart.length; i++) {
            if (this.state.productsInCart[i].id === this.state.product.id) {
                isInCart = true;
            }
        }

        return (
            <form className={"md-form"} onSubmit={this.handleSubmit}>
                <button className="btn btn-primary btn-md my-0 p" type="submit">
                    {isInCart &&
                    "Remove from cart"
                    }
                    {!isInCart &&
                    "Add to cart"
                    }
                    <i className="fas fa-shopping-cart ml-1"></i>
                </button>
                {this.state.error &&
                <div className={"alert alert-danger"}>
                    {this.state.error}
                </div>

                }
            </form>
        )
    }
}

const AddToCartForm = connect(mapStateToProps, mapDispatchToProps)(AddToCartFormR);
export {AddToCartForm}