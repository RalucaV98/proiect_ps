import React from 'react';
import {connect} from "react-redux";
import {constants} from "../../constants";

const mapStateToProps = state => {
    return {products: state.products}
};

class CartComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: props.products
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            products: nextProps.products
        })
    }


    render() {
        const {products} = this.state;

        return (
            <a className="nav-link waves-effect" href={constants.routes.app.cart}>
                <span className="badge blue z-depth-1 mr-1"> {products.length} </span>
                <i className="fas fa-shopping-cart"/>
                <span className="clearfix d-none d-sm-inline-block"> Cart </span>
            </a>
        )
    }
}



const CartMinuature = connect(mapStateToProps)(CartComponent);
export {CartMinuature};