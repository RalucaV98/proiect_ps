import React from 'react';
import {productService} from "../../service";
import {connect} from "react-redux";
import {addProductToCart, fetchCart} from "../../redux/actions";
import {AddToCartForm} from "./AddToCartForm";

const mapStateToProps = state => {
    return {products: state.products}
};

function mapDispatchToProps(dispatch) {
    return {
        fetchInitial: cart => dispatch(fetchCart(cart))
    }
}


class ProductCardMin extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            product: props.product,
            productsInCart: props.products
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({product: nextProps.product, productsInCart: nextProps.products})
    }


    render() {
        const {product} = this.state;
        return (
            <div className="card" style={{height: 400, width: 300, margin: 20}}>
                <div className="view overlay">
                    <img src={"data:image/jpg;base64," + product.base64Photo}
                         className="card-img-top"
                         alt=""/>
                </div>

                <div className="card-body text-center">
                    <h5>
                        <strong>
                            Category:
                        </strong> {product.categoryDto.name}; <br/>
                        <strong>
                            {product.name}
                        </strong>
                    </h5>

                    <h4 className="font-weight-bold blue-text">
                        <strong>{product.price} LEI</strong>
                    </h4>

                    <AddToCartForm product={product}/>

                </div>

            </div>
        )
    }
}

const ProductCard = connect(mapStateToProps, mapDispatchToProps)(ProductCardMin);

export {ProductCard}