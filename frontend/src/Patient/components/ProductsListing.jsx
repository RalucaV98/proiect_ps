import React from 'react';
import {ProductCard} from "./ProductCard";

import './grid-stupid.css'

class ProductsListing extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            products: nextProps.products,
        });
    }

    render() {
        let productsContent = '';
        productsContent = this.state.products.map((product, key) => {
            return (
                <ProductCard product={product} key={key}/>
            )
        });


        return (
            <div className="products-listing">
                {this.state.products !== undefined && this.state.products.length === 0 &&
                <div className={"alert alert-danger"}>
                    No products found
                </div>
                }
                {this.state.products !== undefined && this.state.products.length > 0 &&
                <div className={"grid-container"}>
                    {productsContent}
                </div>
                }

            </div>
        )
    }
}

export {ProductsListing};