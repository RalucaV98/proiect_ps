import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, redirect, hasPermission, ...rest }) => (
    <Route {...rest} render={props => (
        hasPermission
            ? <Component {...props} />
            : <Redirect to={{ pathname: redirect, state: { from: props.location } }} />
    )} />
);