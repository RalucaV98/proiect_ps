const ApiRoutes = function () {
    this.host = "127.0.0.1";
    this.port = 8080;
    this.baseUrl = 'http://' + this.host + ':' + this.port;
    this.loginUrl = this.baseUrl + '/auth/token/';
    this.revokeToken = this.baseUrl + '/auth/revoke/';
    this.loggedUserUrl = this.baseUrl + '/user';

    this.registerUrl = this.baseUrl + "/user/register";
    this.loggedUserCartUrl = this.baseUrl + '/shopping_cart';
    this.productsUrl = this.baseUrl + "/product";
    this.deleteProductUrl = this.productsUrl + "/"; // + "/{id}"


    this.categoryUrl = this.baseUrl + '/category';
    this.deleteCategoryUrl = this.categoryUrl + "/"; // + /{id};

    this.addProductToCartUrl = this.baseUrl + "/shopping_cart/add_product";
    this.removeProductFromCart = this.baseUrl + "/shopping_cart/remove_product";
    this.placeOrder = this.baseUrl + "/order";
    this.veterinaryServices = this.baseUrl + "/facility";
    this.createAppointment = this.baseUrl + "/appointment";


    this.getAllActiveAppointments = this.createAppointment + "/after_now";

    this.generateReportUrl = this.baseUrl + "/admin/report/"; // + "{type};

    this.updateProductImage = this.productsUrl + '/update_image';
};

const AppRoutes = function () {
    this.login = "/login";
    this.register = "/register";
    this.patientBase = "/client";
    this.shop = this.patientBase + "/";
    this.cart = this.patientBase + "/cart";
    this.vetClinic = this.patientBase + "/vet";


    this.doctorBase = "/doctor";
    this.reservationsPage = this.doctorBase + '/reservations';

    this.adminBase = "/admin"; // listare de modele
    this.adminDashboard = this.adminBase + "/dashboard";
    this.categoryBaseCrud = this.adminBase + "/category"; // listare de category
    this.productBaseCrud = this.adminBase + '/product';

    this.notFound = "/not_found";
};

export const constants = {
    routes: {
        api: new ApiRoutes(),
        app: new AppRoutes()
    },
    savedToken: 'savedToken',
    loggedUser: 'loggedUser',

};

