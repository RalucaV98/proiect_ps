import {constants} from "../constants";
import {ROLE_ADMIN, ROLE_DOCTOR, ROLE_PATIENT} from './roles'


export const permissions = {
    isAdmin,
    isDoctor,
    isPatient,
    isLogged
};

function isLogged() {
    const token = JSON.stringify(localStorage.getItem(constants.savedToken));

    return (token);
}

function isPatient() {
    const savedUser = JSON.parse(localStorage.getItem(constants.loggedUser));


    return savedUser !== null && savedUser.role === ROLE_PATIENT;
}

function isDoctor() {
    const savedUser = JSON.parse(localStorage.getItem(constants.loggedUser));

    return savedUser !== null && savedUser.role === ROLE_DOCTOR;
}

function isAdmin() {
    const savedUser = JSON.parse(localStorage.getItem(constants.loggedUser));

    return savedUser !== null && savedUser.role === ROLE_ADMIN;
}