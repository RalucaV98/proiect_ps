import {ADD_PRODUCT, REMOVE_PRODUCT, CLEAR, FETCH_INITIAL} from "../constants/action.types";

export function addProductToCart(product) {
    return {type: ADD_PRODUCT, product};
}

export function removeProductFromCart(product) {
    return {type: REMOVE_PRODUCT, product}
}

export function fetchCart(cart) {
    return {type: FETCH_INITIAL, cart}
}

export function clearCart() {
    return {type: CLEAR};
}