
export const FETCH_INITIAL = "FETCH_INITIAL";
export const ADD_PRODUCT = "ADD_PRODUCT";
export const CLEAR = "CLEAR";
export const REMOVE_PRODUCT = "REMOVE_PRODUCT";