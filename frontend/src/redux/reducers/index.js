import {ADD_PRODUCT, CLEAR, REMOVE_PRODUCT} from "../constants/action.types";
import {FETCH_INITIAL} from "../constants/action.types";

const initialState = {
    products: [],
    cart_id: -1,
};


function rootReducer(state = initialState, action) {
    if (action.type === ADD_PRODUCT) {
        return Object.assign({}, state, {
            products: state.products.concat(action.products),
            cart_id: state.cart_id
        })
    } else if (action.type === FETCH_INITIAL) {
        return Object.assign({}, state, {
            products: action.cart.products,
            cart_id: action.cart.id
        });
    } else if (action.type === CLEAR) {
        return Object.assign({}, state, {
            products: [],
            cart_id: state.cart_id
        })
    } else if (action.type === REMOVE_PRODUCT) {
        return Object.assign({}, state, {
            cart_id: state.cart_id,
            products: state.products.filter((p) => {
                return p.id !== action.product.id
            })
        })
    }
    return state
}

export default rootReducer;