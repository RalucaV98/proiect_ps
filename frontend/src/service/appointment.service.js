import {authHeader, handleResponse} from "../util";
import {constants} from "../constants";


export const appointmentService = {
    getActiveAppointments
};


function getActiveAppointments() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };

    return fetch(constants.routes.api.getAllActiveAppointments, requestOptions).then(handleResponse);
}