import {authHeader, handleResponse} from "../util";
import {constants} from "../constants";


export const categoryService = {
    getAll,
    deleteById,
    save,
};


function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(constants.routes.api.categoryUrl, requestOptions).then(handleResponse)
}


function deleteById(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(constants.routes.api.deleteCategoryUrl + id, requestOptions).then(handleResponse);
}

function save(category) {
    const requestOptions = {
        method: "POST",
        headers: Object.assign(authHeader(), {'Content-type': 'application/json'}),
        body: JSON.stringify(category)
    };

    return fetch(constants.routes.api.categoryUrl, requestOptions).then(handleResponse);
}