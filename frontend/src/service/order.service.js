import {authHeader, handleResponse} from "../util";
import {constants} from "../constants";

export const orderService = {
    placeOrder
};

function placeOrder(cartId) {
    const headers = Object.assign(authHeader());

    console.log(cartId);
    let formData = new FormData();
    formData.append("shoppingCartId", cartId);

    const requestOptions = {
        method: 'POST',
        headers: headers,
        body: formData
    };

    return fetch(constants.routes.api.placeOrder, requestOptions).then(handleResponse);
}