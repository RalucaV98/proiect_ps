import {constants} from "../constants";
import {authHeader, handleResponse} from "../util";
import {cartService} from "./cart.service";


export const productService = {
    getAll,
    addToCart,
    getById,
    removeFromCart,
    deleteById,
    save,
    updateProductImage
};


function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(constants.routes.api.productsUrl, requestOptions).then(handleResponse)
}

function addToCart(shoppingCartId, productId) {
    const headers = Object.assign(authHeader(), {'Content-Type': 'application/json'});

    if (shoppingCartId === -1) {
        return cartService.getLoggedUserCart().then(
            cart => {
                console.log(cart);
                const requestOptions = {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify({
                        shoppingCartId: cart.id,
                        productId
                    })
                };

                return fetch(constants.routes.api.addProductToCartUrl, requestOptions).then(handleResponse);
            }, error => {
                return Promise.reject(error)
            }
        )
    }


    const requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify({
            shoppingCartId,
            productId
        })
    };

    return fetch(constants.routes.api.addProductToCartUrl, requestOptions).then(handleResponse);
}

function removeFromCart(shoppingCartId, productId) {
    const headers = Object.assign(authHeader());

    let formData = new FormData();
    formData.append("shoppingCartId", shoppingCartId);
    formData.append("productId", productId);

    const requestOptions = {
        method: 'POST',
        headers: headers,
        body: formData
    };

    return fetch(constants.routes.api.removeProductFromCart, requestOptions).then(handleResponse);
}


function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(constants.routes.api.productsUrl + '/' + id, requestOptions).then(handleResponse);
}

function deleteById(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(constants.routes.api.deleteProductUrl + id, requestOptions).then(handleResponse);
}

function save(product) {
    const requestOptions = {
        method: "POST",
        headers: Object.assign(authHeader(), {'Content-Type': 'application/json'}),
        body: JSON.stringify(product)
    };

    return fetch(constants.routes.api.productsUrl, requestOptions).then(handleResponse);
}

function updateProductImage(productId, image) {
    var formData = new FormData();
    formData.append("image", image);
    formData.append("productId", productId);

    const requestOptions = {
        method: "POST",
        headers: authHeader(),
        body: formData
    };

    return fetch(constants.routes.api.updateProductImage, requestOptions).then(handleResponse);
}
