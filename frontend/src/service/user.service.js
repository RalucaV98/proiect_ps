import {authHeader, handleResponse} from "../util";
import {constants} from "../constants";


export const userService = {
    login,
    getLoggedUser,
    register,
    logout
};

function login(username, password) {
    const formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);


    const requestOptions = {
        method: 'POST',
        url: constants.routes.api.loginUrl,
        body: formData
    };

    return fetch(requestOptions.url, requestOptions)
        .then(handleResponse)
        .then(
            tokenData => {
                localStorage.clear();
                localStorage.setItem(constants.savedToken, JSON
                    .stringify({
                        token: tokenData.token
                    }));
                return getLoggedUser();
            }
        );
}

function getLoggedUser() {

    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(constants.routes.api.loggedUserUrl, requestOptions).then(handleResponse).then(
        user => {
            localStorage.setItem(constants.loggedUser, JSON.stringify(user));
            return user;
        }
    )
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(constants.routes.api.registerUrl, requestOptions).then(handleResponse);
}



function logout() {
    const authData = JSON.parse(localStorage.getItem(constants.savedToken));

    const requestOptions = {
        method: 'POST',
        url: constants.routes.api.revokeToken,
        headers: authHeader(),
    };

    return fetch(requestOptions.url, requestOptions).then(handleResponse).then(
        success => {
            localStorage.clear();
            window.location.href = constants.routes.app.login;
        }
    )
}