import {authHeader, handleResponse} from "../util";
import {constants} from "../constants";


export const vetClinicService = {
    getAllVeterinaryServices,
    createAppointment
};


function getAllVeterinaryServices() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(constants.routes.api.veterinaryServices, requestOptions).then(handleResponse);
}

function createAppointment(appointment) {
    let headers = Object.assign(authHeader(), {'Content-type': 'application/json'});

    const requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(appointment)
    };

    return fetch(constants.routes.api.createAppointment, requestOptions).then(handleResponse);
}