
import {constants} from "../constants";

export function authHeader() {
    let savedToken = JSON.parse(localStorage.getItem(constants.savedToken));
    if (savedToken && savedToken.token) {
        return {
            'Token-key': savedToken.token
        }
    }
    return {}
}