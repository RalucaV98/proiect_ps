

export function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (!response.ok) {

            if (data.status === 401 && data.message === "Invalid token") {
                localStorage.clear();
                window.location.reload();
            }

            return Promise.reject(data);
        }

        return data;
    });
}